module(..., package.seeall)

require "sprite"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
local CSX = display.contentScaleX
local CSY = display.contentScaleY

scaleFactor = tonumber(string.format("%.1f", 1 / CSX))

width = math.floor((CW - SOX * 2) * scaleFactor + 0.5)
height = math.floor((CH - SOY * 2) * scaleFactor + 0.5)

if scaleFactor == 1 then       -- iPhone, myTouch 320x480
    imageSuffix = ""
elseif scaleFactor == 1.5 then -- Nexus 480x800, Droid 480x840
    imageSuffix = "_480"
elseif scaleFactor == 2 then    -- iPhone 4 640x960
    imageSuffix = "_640"
else -- 1.7 (HTC Sensation 540x960), 1.9 (Galaxy Tab 600x1024), 2.5 (Galaxy Nexus, Xoom 800x1280)
    imageSuffix = "_480"
    CSX = 2/3 -- seems to be a 'magic' number
    CSY = 2/3
end

function getCSX()
    return CSX
end

function updateText(obj, text, x, y, ref)
    obj:setReferencePoint(display.CenterReferencePoint)
    obj.xScale = 1
    obj.yScale = 1
    obj.text = text
    obj:setReferencePoint(ref or display.TopLeftReferencePoint)
    obj.x = x
    obj.y = y
    obj:scale(display.contentScaleX, display.contentScaleY)
end

function newScaledImage(name)
    local image = display.newImage("images/" .. name .. imageSuffix .. ".png", true)
    image:scale(CSX, CSY)
    return image
end

function newScaledText(string, x, y, font, size)
    local text = display.newText(string, x, y, font, size * scaleFactor)
    text:scale(CSX, CSY)
    return text
end

function newMultiLineText(strings, x, y, font, size, ref)
    local lines = display.newGroup()
    for i = 1, #strings do
        local line = newScaledText("", 0, 0, font, size)
        line:setTextColor(0, 0, 0)
        updateText(line, strings[i], 0, (i == 1) and 0 or ((i - 1) * lines[1].height * 0.7))
        lines:insert(line)
    end
    lines:setReferencePoint(ref or display.TopLeftReferencePoint)
    lines.x = x
    lines.y = y
    return lines
end

function loadSpriteSheet(name, suffix)
    if type(suffix) ~= "string" then
        suffix = imageSuffix
    end

    local ss = {}

    ss.moduleName = name .. suffix
    ss.module = require(ss.moduleName)
    ss.spriteSheetData = ss.module.getSpriteSheetData()
    ss.frameCount = #ss.spriteSheetData.frames
    ss.spriteSheet = sprite.newSpriteSheetFromData(
        "images/" .. name .. suffix .. ".png",
        ss.spriteSheetData
    )
    ss.spriteSet = sprite.newSpriteSet(ss.spriteSheet, 1, 1)

    function ss:newSprite(rescale)
        local s = sprite.newSprite(self.spriteSet)

        -- rescale by default, unless explicitly specified not to
        if rescale ~= false then
            s:scale(CSX, CSY)
        end

        return s
    end

    function ss:add(...)
        sprite.add(self.spriteSet, ...)
    end

    function ss:dispose()
        self.spriteSheet:dispose()
        package.loaded[self.moduleName] = nil
    end

    return ss
end

-- creates a rectangle that covers the entire screen
function newFullRect(blockEvents, hitTestable)
    local fullRect = display.newRect(SOX, SOY, CW - SOX * 2, CH - SOY * 2)
    fullRect:setReferencePoint(display.CenterReferencePoint)
    fullRect:setFillColor(0, 0, 0, 255)
    fullRect.alpha = 0

    function fullRect:clean()
        self:removeEventListener("tap", self)
    end

    function fullRect:touch()
        return true
    end

    function fullRect:tap()
        return true
    end

    if blockEvents then
        fullRect:addEventListener("touch", fullRect)
        fullRect:addEventListener("tap", fullRect)
    end

    fullRect.isHitTestable = not not hitTestable

    return fullRect
end

function newTouchImage(name, onTouch)
    local image = newScaledImage(name)

    function image:touch(e)
        if e.phase == "began" then
            timer.performWithDelay(0, onTouch)
        end
        return true
    end
    image:addEventListener("touch", image)

    return image
end

function newButton(params)
    local button = display.newGroup()

    local default, over, text

    default = type(params.default) == "string" and newScaledImage(params.default) or params.default
    default.originalScale = default.xScale
    button:insert(default)

    params.defaultScale = params.defaultScale or 1
    params.overScale = params.overScale or 1.1

    local function scaleDefault(scale)
        if scale then
            default.xScale = scale * default.originalScale
            default.yScale = scale * default.originalScale
        end
    end

    if params.over then
        over = type(params.over) == "string" and newScaledImage(params.over) or params.over
        over.x = default.x
        over.y = default.y
        over.isVisible = false
        button:insert(over)
    else
        scaleDefault(params.defaultScale)
    end

    button:setReferencePoint(display.CenterReferencePoint)

    function button:updateText(newText)
        updateText(text, newText,
            self.xReference + (params.textOffsetX or 0),
            self.yReference + (params.textOffsetY or 0),
            display.CenterReferencePoint)
    end

    if params.text then
        local fontName = params.fontName or native.systemFont
        local fontSize = params.fontSize or button.contentHeight * 0.5
        text = newScaledText("", 0, 0, fontName, fontSize)
        button:updateText(params.text)
--        updateText(text, params.text,
--            button.xReference + (params.textOffsetX or 0),
--            button.yReference + (params.textOffsetY or 0),
--            display.CenterReferencePoint)
        text:setTextColor(0, 0, 0)
        button:insert(text)
    end

    function button:touch(e)
        local bounds = self.contentBounds
        local isWithinBounds =
			bounds.xMin <= e.x and bounds.xMax >= e.x and bounds.yMin <= e.y and bounds.yMax >= e.y

        if "began" == e.phase then
            self.isDragging = true
            if over then
                default.isVisible = false
                over.isVisible = true
            else
                scaleDefault(params.overScale)
            end
            display.getCurrentStage():setFocus(self)
        elseif "ended" == e.phase or "cancelled" == e.phase then
            if self.isDragging then
                self.isDragging = false
            else
                return false
            end
            if over then
				default.isVisible = true
				over.isVisible = false
            else
                scaleDefault(params.defaultScale)
            end

            if "ended" == e.phase then
				-- Only consider this a "click" if the user lifts their finger inside button's contentBounds
				if isWithinBounds then
                    if params.onTap then
                        -- perform with delay to let the touch function return "true"
                        -- before calling onTap
                        timer.performWithDelay(0, function()
                            params.onTap({target = self})
                        end)
                    end
				end
            end

            display.getCurrentStage():setFocus(nil)
        elseif "moved" == e.phase then
            if not self.isDragging then
                return false
            end
            if over then
                default.isVisible = not isWithinBounds
				over.isVisible = isWithinBounds
            else
                scaleDefault(isWithinBounds and params.overScale or params.defaultScale)
            end
        end

        return true
    end

    button:addEventListener("touch", button)

    return button
end

function newToggle(params)
    local toggle = display.newGroup()
    local onButton, offButton

    function toggle:switch(isOn)
        onButton.isVisible = isOn
        offButton.isVisible = not isOn
    end
    local function onTap()
        toggle:switch(not onButton.isVisible)
        if params.onTap then
            params.onTap(onButton.isVisible)
        end
    end

    onButton = newButton({
        default = params.defaultOn,
        over = params.overOn,
        defaultScale = params.defaultScale,
        overScale = params.overScale,
        text = params.onText,
        textOffsetX = params.textOffsetX,
        textOffsetY = params.textOffsetY,
        fontName = params.fontName,
        fontSize = params.fontSize,
        onTap = onTap
    })
    offButton = newButton({
        default = params.defaultOff,
        over = params.overOff,
        defaultScale = params.defaultScale,
        overScale = params.overScale,
        text = params.offText,
        textOffsetX = params.textOffsetX,
        textOffsetY = params.textOffsetY,
        fontName = params.fontName,
        fontSize = params.fontSize,
        onTap = onTap
    })

    toggle:insert(onButton)
    toggle:insert(offButton)

    toggle:switch(not not params.isOn)

    toggle:setReferencePoint(display.CenterReferencePoint)

    return toggle
end

function newSlider(params)
    local slider = display.newGroup()
    local slides = display.newGroup()
    local buttons = display.newGroup()

    slider:insert(slides)
    slider:insert(buttons)

    slides.slideIndex = 1

    function slides:getSlideIndex(x)
        if not x then x = self.x end
        return 1 + math.floor( (-x + CW * 0.5) / CW )
    end

    function slides:slideTo(slideIndex, animate)
        self:cancelSlideTween()

        local x = -(slideIndex - 1) * CW
        if animate then
            self.slideTween = transition.to(self, {x = x, time = 1500, transition = easing.outExpo})
        else
            self.x = x
        end

        self.slideIndex = self:getSlideIndex(x)
        buttons:toggle(self.slideIndex)
    end

    function slider:slideTo(...)
        slides:slideTo(...)
    end

--    function slider:getSlideIndex()
--        return slides.slideIndex
--    end

--    function slider:getSlides()
--        return slides
--    end
--
--    function slider:getButtons()
--        return buttons
--    end

    local function onTouchButton(e)
        if "began" == e.phase then
            slides:slideTo(e.target.id, true)
        end
        return true
    end

    function buttons:toggle(index)
        local button, isActive
        for i = 1, buttons.numChildren do
            button = buttons[i]
            isActive = i == index
            button[1].isVisible = isActive
            button[2].isVisible = not isActive
        end
    end

    function slider:addSlide(slide, button)
        slide:setReferencePoint(display.CenterReferencePoint)
        slide.x = (CW / 2) + CW * slides.numChildren
        slides:insert(slide)
        if button then
            buttons:insert(button)
            button:addEventListener("touch", onTouchButton)
            buttons:toggle(1)
        end
    end

    function slides:cancelSlideTween()
        if self.slideTween then
            transition.cancel(self.slideTween)
            self.slideTween = nil
        end
    end

    function slides:startDrag(e)
        display.getCurrentStage():setFocus(self)
        self.isDragging = true

        self:cancelSlideTween()

        self.xStart = e.x
        self.xOffset = self.x
        self.lastTime = system.getTimer()
    end

    function slides:endDrag(e)
        if self.isDragging then
            self.isDragging = false
        else
            return
        end

        display.getCurrentStage():setFocus(nil)

        local slideIndex = self.slideIndex
        local xShift, xShiftAbs

        xShift = self.x - self.xOffset
        xShiftAbs = math.abs(xShift)

        local deltaTime = system.getTimer() - self.lastTime
        if deltaTime < 10 then deltaTime = 10 end
        local speed = xShiftAbs / (0.001 * deltaTime)

        if xShiftAbs > 50 or speed > 150 then
            slideIndex = self.slideIndex - xShift / xShiftAbs
        elseif xShiftAbs > 10 then
            -- user didn't drag far enough - so we use default slideIndex (return to current slide)
        elseif params and params.onSelect then
            timer.performWithDelay(0, function()
                params.onSelect(slides.slideIndex)
            end)
        end

        -- min/max slide index
        if slideIndex <= 0 or slideIndex > self.numChildren then
            slideIndex = self.slideIndex
        end

        self:slideTo(slideIndex, true)
    end

    function slides:drag(e)
        if not self.isDragging then
            return
        end

        self.x = self.xOffset + e.x - self.xStart
    end

    function slides:touch(e)
        if "began" == e.phase then
            self:startDrag(e)
        elseif "ended" == e.phase or "cancelled" == e.phase then
            self:endDrag(e)
        else
            self:drag(e)
        end
        return true
    end

    slides:addEventListener("touch", slides)

    return slider
end

-- FPS and performance counter

--local fps = 0
--local fpsTime = 0
--
--local fpsText = newScaledText("00", 10, 10 + SOY, native.systemFont, 12)
--local memText = newScaledText("00", 10, 24 + display.screenOriginY, native.systemFont, 12)
--local texMemText = newScaledText("00", 10, 38 + display.screenOriginY, native.systemFont, 12)
--
--local textRect = display.newRoundedRect(CW / 2 - 80, CH - SOY - 48, 160, 46, 9)
--textRect:setFillColor(50, 50, 50, 200)
--
--local textGroup = display.newGroup()
--
--textGroup:insert(textRect)
--textGroup:insert(fpsText)
--textGroup:insert(memText)
--textGroup:insert(texMemText)
--
--local function showFPS(e)
--     fps = fps + 1
--     if e.time - fpsTime > 1000 then
--         fpsTime = e.time
--         local x = CW / 2 - 60
--         updateText(fpsText, "FPS: " .. tostring(fps), x, CH - SOY - 30,
--             display.BottomLeftReferencePoint)
--         updateText(memText, "Memory: " .. string.format("%.2f Kb", collectgarbage("count")),
--             x, CH - SOY - 16, display.BottomLeftReferencePoint)
--         updateText(texMemText,
--             "Textures: " .. string.format("%.2f Mb", system.getInfo("textureMemoryUsed") / 1048576),
--             x, CH - SOY - 2, display.BottomLeftReferencePoint)
--
--         textGroup:toFront()
--
--         fps = 0
--     end
--end
--
--Runtime:addEventListener("enterFrame", showFPS)

-------------------------------