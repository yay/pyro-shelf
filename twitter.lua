module(..., package.seeall)

require "oAuth"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY

consumer_key = "DpltiZAbpx93QUNippbrg"
consumer_secret = "m6oMPB1rAZW1pLmxsM7x9fL0MrqfyJH2sOBDLLVhoc"
local access_token
local access_token_secret
local user_id
local screen_name

-- your web address below can be anything from what I can make out
-- as long as it is the same as the callback url set in twitter settings
-- twitter sends the web address with the token back to your app
-- and the code strips out the token to use to authorise it
-- doing it this way, means the web popup closes itself - if it doesn't it means
-- there is some kind of problem with the code
-- I found that out the hard way!!

local twitter_request = (oAuth.getRequestToken(
    consumer_key,
    "http://pyroshelf.com",
    "http://twitter.com/oauth/request_token",
    consumer_secret
))
print("twitter_request: ", twitter_request)
local twitter_request_token = twitter_request.token
local twitter_request_token_secret = twitter_request.token_secret

local function listener(event)
    print("listener")
    local remain_open = true
    local url = event.url

    if url:find("oauth_token") then

        url = url:sub(url:find("?") + 1, url:len())

        local authorize_response = responseToTable(url, {"=", "&"})
        remain_open = false

        print("authorize_response.oauth_token: ", authorize_response.oauth_token)
        print("authorize_response.oauth_verifier: ", authorize_response.oauth_verifier)
        print("twitter_request_token_secret: ", twitter_request_token_secret)
        local ppp = oAuth.getAccessToken(
            authorize_response.oauth_token,
            authorize_response.oauth_verifier,
            twitter_request_token_secret,
            consumer_key, consumer_secret,
            "https://api.twitter.com/oauth/access_token"
        )
        print("ppp: ", ppp)

        local access_response = responseToTable(
            oAuth.getAccessToken(
                authorize_response.oauth_token,
                authorize_response.oauth_verifier,
                twitter_request_token_secret,
                consumer_key, consumer_secret,
                "https://api.twitter.com/oauth/access_token"
            ),
            {"=", "&" }
        )

        access_token = access_response.oauth_token
        access_token_secret = access_response.oauth_token_secret
        user_id = access_response.user_id
        screen_name = access_response.screen_name

        print("access_token: ", access_token)
        print("access_token_secret: ", access_token_secret)
        print("user_id: ", user_id)
        print("screen_name: ", screen_name)

        -- API CALL:
        -- change the message posted
        local params = {}
        params[1] =
        {
            key = 'status',
            value = "I'm playing Pyro Shelf points on #PixelSlice! http://itunes.apple.com/us/app/pixel-slice/id436617489?mt=8"
        }

        request_response = oAuth.makeRequest(
            "http://api.twitter.com/1/statuses/update.json",
            params,
            consumer_key,
            access_token,
            consumer_secret,
            access_token_secret,
            "POST"
        )
        print("Request response: ", request_response)

    end

    return remain_open
end

--this is your web popup, change position/size as you wish
function tweetit()
    local margin = 10
    native.showWebPopup(SOX + margin, SOY + margin, CW - SOX * 2 - margin * 2, CH - SOY * 2 - margin * 2,
        "http://api.twitter.com/oauth/authorize?oauth_token=" .. twitter_request_token,
        {urlRequest = listener})
end

-- I use this for testing on a mac, but lack of text field
-- makes it difficult for my app, I may as well use device
-- you could use a random message generator for testing purposes
-- so as to send a unique message each time, would let you see messages in terminal then

-- system.openURL(10, 20, 300, 450, "http://api.twitter.com/oauth/authorize?oauth_token=" .. twitter_request_token, {urlRequest = listener})

-- this is the bit that strips the token from the web address returned

-- RESPONSE TO TABLE

function responseToTable(str, delimeters)
    local obj = {}

    while str:find(delimeters[1]) ~= nil do
        if #delimeters > 1 then
            local key_index = 1
            local val_index = str:find(delimeters[1])
            local key = str:sub(key_index, val_index - 1)

            str = str:sub((val_index + delimeters[1]:len()))

            local end_index
            local value

            if str:find(delimeters[2]) == nil then
                end_index = str:len()
                value = str
            else
                end_index = str:find(delimeters[2])
                value = str:sub(1, (end_index - 1))
                str = str:sub((end_index + delimeters[2]:len()), str:len())
            end
            obj[key] = value
            print(key .. ":" .. value)
        else

            local val_index = str:find(delimeters[1])
            str = str:sub((val_index + delimeters[1]:len()))

            local end_index
            local value

            if str:find(delimeters[1]) == nil then
                end_index = str:len()
                value = str
            else
                end_index = str:find(delimeters[1])
                value = str:sub(1, (end_index - 1))
                str = str:sub(end_index, str:len())
            end
            obj[#obj + 1] = value
            print(value)
        end
    end
    return obj
end