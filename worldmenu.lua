module(..., package.seeall)

require "settings"
require "screen"
require "sound"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
--local CSX = display.contentScaleX
--local CSY = display.contentScaleY

local CSX = screen.getCSX()
local CSY = CSX

local newScaledImage = screen.newScaledImage
local newScaledText = screen.newScaledText
local updateText = screen.updateText

--local function onUnlockAlert(e)
--    if "clicked" == e.action then
--        if e.index == 1 then
--            sound.play("click")
--            director:changeScene("earn")
--        end
--    end
--end

function new(index, goToNext)

    local menuGroup = display.newGroup()

    local bg = newScaledImage("main_bg")
    bg:setReferencePoint(display.TopCenterReferencePoint)
    bg.x = CW / 2
    bg.y = SOY

    local totalText = newScaledText("", 0, 0, settings.mainFont, 20)
    updateText(totalText, "Total: " .. tostring(settings.data.gameScore),
        CW - SOX - 10, CH - SOY - 34, display.BottomRightReferencePoint)

    local picklocksText = screen.newScaledText("", 0, 0, settings.mainFont, 20)
    updateText(picklocksText, "Picklocks: " .. settings.data.picklocks,
        CW - SOX - 10, CH - SOY - 10, display.BottomRightReferencePoint)

    local backBtn = screen.newButton({
        default = "back",
        overScale = 1.2,
        onTap = function()
            sound.play("click")
            director:changeScene("mainmenu")
        end
    })
    backBtn:setReferencePoint(display.BottomLeftReferencePoint)
    backBtn.x = 10
    backBtn.y = CH - SOY - 10

    local worlds = settings.data.worlds

    local slider = screen.newSlider({
        onSelect = function(slideIndex)
            -- if world exists and unlocked
            if slideIndex <= #worlds and worlds[slideIndex].score >= 0 then
                sound.play("click")
                director:changeScene("levelmenu", slideIndex)
            end
        end
    })

    local numSlides = #worlds + 1
    local dotSpacing = 64

    local ref = display.CenterReferencePoint

    local function newWorldText(params)
        local lg = display.newGroup()

        local rectHeight = 36
        local textRect = display.newRect(0, -rectHeight / 2, 172, rectHeight)
        textRect:setFillColor(0, 0, 0, 255)
        textRect.x = 0
        textRect.y = 0
        textRect.alpha = params.rectAlpha

        local worldText = newScaledText("", 0, 0, settings.mainFont, params.fontSize or 24)
        updateText(worldText, params.text, textRect.xOrigin, textRect.yOrigin, ref)

        lg:insert(textRect)
        lg:insert(worldText)

        lg:setReferencePoint(ref)
        lg.x = params.x
        lg.y = params.y

        return lg
    end

    -- table to save references to display objects involved in the unlocking animation
    local lockTable = {}
    -- picklocks required to unlock a world
    local minPicklocks = 100

    local function unlockWorld(worldIndex)
        if worlds[worldIndex].score < 0 then
            worlds[worldIndex].score = 0
            settings.data.picklocks = settings.data.picklocks - minPicklocks
            updateText(picklocksText, "Picklocks: " .. settings.data.picklocks,
                CW - SOX - 10, CH - SOY - 10, display.BottomRightReferencePoint)
        end

        function slider:clean()
            if self.slideFxTimer then
                timer.cancel(self.slideFxTimer)
            end
        end

        local lockEntry = lockTable[worldIndex]
        if lockEntry then
            local lock = lockEntry.lock
            local scores = lockEntry.scores
            local button = lockEntry.button

            function lock:clean()
                if self.fxTween then
                    transition.cancel(self.fxTween)
                end
            end

            lock.fxTween = transition.to(lock, {time=2000, alpha=0,
                transition=easing.outQuad,
                xScale=lock.xScale*2, yScale=lock.yScale*2,
                onStart=function()
                    sound.play("unlock")
                    button:updateText("SELECT")
                end,
                onComplete=function()
                    lock:removeSelf()
                end}
            )

            function scores:clean()
                if self.fxTween then
                    transition.cancel(self.fxTween)
                end
            end

            scores.fxTween = transition.to(scores, {time=1000, delay=1000, alpha=1})
        end
    end

    local slidesData = {
        {name = "Delighted", rectColor = {0, 135, 220}},
        {name = "Concerned", rectColor = {42, 191, 7}},
        {name = "Annoyed", rectColor = {255, 206, 0}},
        {name = "Fiery", rectColor = {210, 0, 0}},
    }

    for i = 1, numSlides do
        local slideGroup = display.newGroup()

        local slideImg = newScaledImage("slide")

        -- an entry of the lockTable
        local lockEntry

        if i < numSlides then
            local world = worlds[i]

            local slideRect = display.newRoundedRect(0, 0,
            slideImg.width * CSX - 30,
            slideImg.height * CSY - 30, 8)
            slideRect.x = slideImg.x
            slideRect.y = slideImg.y

            slideGroup:insert(slideRect)

            local slideData = slidesData[i]
            slideRect:setFillColor(unpack(slideData.rectColor))

            local scoreGroup = display.newGroup()
            scoreGroup:insert(newWorldText{
                text = tostring(world.progress) .. "/" .. tostring(#world.levels),
                x = slideImg.x,
                y = slideImg.y,
                rectAlpha = 0.4
            })
            scoreGroup:insert(newWorldText{
                -- world score can be "-1" for locked world
                -- since during unlock animation we don't update this text, we do a check here
                text = "Score: " .. tostring(world.score < 0 and 0 or world.score),
                fontSize = 20,
                x = slideImg.x,
                y = slideImg.y + 70,
                rectAlpha = 0.4
            })
            slideGroup:insert(scoreGroup)

            if world.score < 0 then
                -- hide scoreGroup to reveal it later on world unlock animation
                scoreGroup.alpha = 0

                local lock = newScaledImage("big_lock")
                lock.x = slideImg.x
                lock.y = slideImg.y + 36
                slideGroup:insert(lock)

                lockEntry = {
                    lock = lock,
                    scores = scoreGroup
                }
                lockTable[i] = lockEntry
            end

            slideGroup:insert(newWorldText{
                text = tostring(i) .. ". " .. slideData.name,
                x = slideImg.x,
                y = slideImg.y - 70,
                rectAlpha = 0.6
            })

        else
            local newLevelsText = newScaledText("", 0, 0, settings.mainFont, 24)
            local comingSoonText = newScaledText("", 0, 0, settings.mainFont, 24)
            updateText(newLevelsText, "New levels", slideImg.x, slideImg.y - 30, ref)
            updateText(comingSoonText, "coming soon!", slideImg.x, slideImg.y, ref)
            slideGroup:insert(newLevelsText)
            slideGroup:insert(comingSoonText)
        end

        local unlockBtn = screen.newButton{
            default = "unlock",
            text = i < numSlides and (worlds[i].score < 0 and "UNLOCK" or "SELECT") or "FOLLOW US",
            fontSize = 20,
            fontName = settings.mainFont,
            textOffsetY = settings.isDevice and 0 or 1,
            onTap = function(e)
                if i < numSlides then
                    if worlds[i].score < 0 then
                        if worlds[i-1].score < 0 then
                            sound.play("locked")
                            native.showAlert("Not so fast!", "You can only unlock worlds in order.", {"OK"})
                        else
                            if settings.data.picklocks >= minPicklocks then
                                unlockWorld(i)
                            else
                                sound.play("locked")
                                native.showAlert("Whoops!",
                                    "You don't have enough picklocks.\n\n" ..
                                    tostring(minPicklocks) .. " picklocks required.", {"OK"})
                            end
                        end
                    else
                        sound.play("click")
                        director:changeScene("levelmenu", i)
                    end
                else
                    sound.play("click")
                    system.openURL("http://www.facebook.com/PyroShelf")
                end
            end
        }
        unlockBtn.x = slideImg.x
        unlockBtn.y = slideImg.y + 160
        slideGroup:insert(unlockBtn)
        -- add unlock button to lockEntry to change text from UNLOCK to SELECT later
        if lockEntry then
            lockEntry.button = unlockBtn
        end

        slideGroup:insert(slideImg)

        slideGroup:setReferencePoint(display.CenterReferencePoint)
        slideGroup.y = CH / 2 - 20

        local buttonOn = newScaledImage("dot_on")
        local buttonOff = newScaledImage("dot_off")
        local buttonGroup = display.newGroup()
        buttonGroup.id = i
        buttonOff:scale(0.8, 0.8)
        buttonGroup:insert(buttonOn)
        buttonGroup:insert(buttonOff)
        buttonGroup:setReferencePoint(display.CenterReferencePoint)
        buttonGroup.x = (CW - (numSlides - 1) * dotSpacing) / 2 + (i - 1) * dotSpacing
        buttonGroup.y = CH / 2 + 160 - SOY / 2

        slider:addSlide(slideGroup, buttonGroup)
    end

    menuGroup:insert(bg)
    menuGroup:insert(totalText)
    menuGroup:insert(picklocksText)
    menuGroup:insert(slider)
    menuGroup:insert(backBtn)

    if index then
        slider:slideTo(index)

        local nextIndex = index + 1
        if goToNext and nextIndex <= #worlds then
            slider.slideFxTimer = timer.performWithDelay(500, function()
                slider:slideTo(nextIndex, true)
            end)
        end
    else
        local lastUnlocked = 1
        for i = 2, #worlds do
            if worlds[i].score >= 0 then
                lastUnlocked = i
            else
                break
            end
        end
        slider:slideTo(lastUnlocked)
    end

    return menuGroup
end

function clean()
end