module(..., package.seeall)

require "physics"
require "counter"
require "bomb"

physics.start()
physics.setGravity(0, 0)

-----------------------------------------------------------------

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY

local levelData

local function createFadeRect()
    local fadeRect = display.newRect(SOX, SOY, CW - SOX * 2, CH - SOY * 2)
    fadeRect:setFillColor(0, 0, 0, 255)
    fadeRect.alpha = 0
    fadeRect.isVisible = false

    function fadeRect:touch()
        return true
    end
    fadeRect:addEventListener("touch", fadeRect)

    return fadeRect
end

local function createPauseMenu(fadeRect)
    local menuBusy = false
    local effectTime = 333

    local lg = display.newGroup()

    local slidePanel = display.newGroup()

    local rect = display.newRect(0, SOY, 100, CH - SOY * 2)
    rect:setFillColor(100, 100, 100, 170)

    local xOffscreen = SOX - rect.width

    local refresh = display.newImageRect("images/refresh.png", 48, 48)
    refresh.x = rect.width / 2
    refresh.y = 50

    local list = display.newImageRect("images/list.png", 48, 48)
    list.x = rect.width / 2
    list.y = CH - 50

    local pause = display.newImageRect("images/pause.png", 48, 48)
    pause:setReferencePoint(display.TopRightReferencePoint)
    pause.x = CW - 10
    pause.y = 10 + SOY

    function lg:showMenu()
        if not menuBusy then
            menuBusy = true
            fadeRect.isVisible = true
            transition.to(fadeRect, {time = effectTime, alpha = 0.7})
            transition.to(slidePanel, {time = effectTime, x = SOX, onComplete=function()
                menuBusy = false
            end})
        end
    end

    function lg:hideMenu()
        if not menuBusy then
            menuBusy = true
            transition.to(slidePanel, {time = effectTime, x = xOffscreen})
            transition.to(fadeRect, {time = effectTime, alpha = 0, onComplete=function()
                fadeRect.isVisible = false
                physics.start()
                menuBusy = false
            end})
        end
    end

    function slidePanel:touch(e)
        if e.phase == "began" then
            lg:hideMenu()
        end
        return true
    end
    slidePanel:addEventListener("touch", slidePanel)

    function refresh:touch(e)
        if e.phase == "began" and not menuBusy then
            director:changeScene(_NAME, levelData)
        end
        return true
    end
    refresh:addEventListener("touch", refresh)

    function list:touch(e)
        if e.phase == "began" and not menuBusy then
            director:changeScene("editor", levelData)
        end
        return true
    end
    list:addEventListener("touch", list)

    function pause:touch(e)
        if e.phase == "began" then
--            physics.pause()
            lg:showMenu()
        end
        return true
    end
    pause:addEventListener("touch", pause)

    slidePanel:insert(rect)
    slidePanel:insert(refresh)
    slidePanel:insert(list)

    lg:insert(pause)
    lg:insert(slidePanel)

    slidePanel.x = xOffscreen

    return lg
end

local function createWalls()
    local thickness = 20
    local name = "wall"

    local height = CH - SOY * 2
    local width  = CW - SOX * 2

    local lg = display.newGroup()

    local leftWall = display.newRect(SOX - thickness, SOY, thickness, height)
    local rightWall = display.newRect(CW - SOX, SOY, thickness, height)
    local topWall = display.newRect(SOX, SOY - thickness, width, thickness)
    local bottomWall = display.newRect(SOX, CH - SOY, width, thickness)

    leftWall.name = name
    rightWall.name = name
    topWall.name = name
    bottomWall.name = name

    physics.addBody(leftWall, "static")
    physics.addBody(rightWall, "static")
    physics.addBody(topWall, "static")
    physics.addBody(bottomWall, "static")

    lg:insert(leftWall)
    lg:insert(rightWall)
    lg:insert(topWall)
    lg:insert(bottomWall)

    return lg
end

local function createBombs(grid)
    local lg = display.newGroup()

    local xBlocks = 5
    local yBlocks = 6

    local blockWidth  = display.contentWidth  / xBlocks
    local blockHeight = blockWidth

    local cx, cy, block, blockIndex

    local function canTouch()
        return true
    end

    for i = 1, xBlocks do
        for j = 1, yBlocks do
            cx = blockWidth * i - blockWidth / 2
            cy = blockHeight * j - blockHeight / 2

            blockIndex = (j - 1) * xBlocks + i
            block = grid[ blockIndex ]

            if block > 0 then
                lg:insert(bomb.new({
                    x = cx,
                    y = cy + 50,
                    state = block,
                    canTouch = canTouch
                }))
            end
        end
    end

    return lg
end

function new(data)

    levelData = data
    counter.reset(3)

    -- creating a local group to put everything into and return
    local lg = display.newGroup()

    lg:insert(createBombs(levelData.bombGrid))
    lg:insert(createWalls())

    local fadeRect = createFadeRect()
    lg:insert(fadeRect)

    local pauseMenu = createPauseMenu(fadeRect)
    lg:insert(pauseMenu)

    return lg
end