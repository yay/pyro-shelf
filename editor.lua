module(..., package.seeall)

require "sprite"
require "screen"
local json = require "json"
require "network"
require "settings"
local url = require "socket.url"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY

local function newSprite(self, rescale)
    local s = sprite.newSprite(self.spriteSet)

    -- rescale by default, unless explicitly specified not to
    if rescale ~= false then
        s.xScale = display.contentScaleX
        s.yScale = display.contentScaleY
    end

    return s
end

local seqCount = 5

local levelData

local ssBlock = screen.loadSpriteSheet("block_ss")
for i = 1, seqCount do
    ssBlock:add(tostring(i), i, 1, 100, 1)
end

local function newSlider(max, v, onChange)
--    local minValue = min
--    local maxValue = max
    local segWidth = CW / max

    local lg = display.newGroup()

    if v then
        lg.theValue = v
    else
        lg.theValue = 3 -- default value
    end

    local ruler = display.newRect(0, SOY, CW, 20)
    local prog = display.newRect(0, SOY, 0, 20)
    local text = screen.newScaledText("0", CW/2, SOY, settings.mainFont, 16)
    ruler:setFillColor(0, 100, 0)
    prog:setFillColor(0, 150, 0)

    local function upd()
--        print("Slider:", lg.theValue)
        prog.width = segWidth * lg.theValue
        prog:setReferencePoint(display.TopLeftReferencePoint)
        prog.x = 0
        screen.updateText(text, tostring(lg.theValue), CW/2, SOY)
    end

    function ruler:touch(e)
        if e.phase == "began" or e.phase == "moved" then
            local tmp = math.floor(e.x / segWidth + 0.5)
            if tmp ~= lg.theValue and tmp > 0 then
                lg.theValue = tmp
                onChange(tmp)
--                levelData.maxTouches = tmp
                upd()
            end
        end
    end

    ruler:addEventListener("touch", ruler)

    lg:insert(ruler)
    lg:insert(prog)
    lg:insert(text)

    upd()

    return lg
end

local function newBlock(grid, index)
    local block = ssBlock:newSprite()
    block.name = "block"

    block.grid = grid
    block.index = index

    function block:init()
        local seqIndex = self.grid[self.index] + 1
        block:prepare(tostring(seqIndex))
        block:play()
    end
    block:init()

    function block:update()
        local seqIndex = tonumber(self.sequence)
        self.grid[self.index] = seqIndex - 1
        -- for copying (remove for real crafting)
        if self.grid[self.index] == 2 then
            self.grid[self.index] = 3
        elseif self.grid[self.index] == 3 then
            self.grid[self.index] = 2
        end
    end

    function block:touch(e)
        if e.phase == "began" then
            if self.sequence == tostring(seqCount) then
                self:prepare("1")
            else
                self:prepare(tonumber(self.sequence) + 1)
            end
            self:update()
        end
        return true
    end
    block:addEventListener("touch", block)

    return block
end

local function createBlocks(grid)
    local lg = display.newGroup()

    local xBlocks = 5
    local yBlocks = 6

    local blockWidth  = display.contentWidth  / xBlocks
    local blockHeight = blockWidth

    local cx, cy, block, blockIndex

    for i = 1, xBlocks do
        for j = 1, yBlocks do
            cx = blockWidth * i - blockWidth / 2
            cy = blockHeight * j - blockHeight / 2

            blockIndex = (j - 1) * xBlocks + i

            block = newBlock(grid, blockIndex)
            block.x = cx
            block.y = cy + 50

            lg:insert(block)
        end
    end

    return lg
end

local function resetBlocks(group)
    for i = 1, group.numChildren do
        local child = group[i]
        if child.name == "block" then
            child:prepare("1")
            child:update()
            child:play()
        end
    end
end

function new(data)
    local lg = display.newGroup()

    -- creating background rect
    local bgRect = display.newRect(SOX, SOY, CW - SOX * 2, CH - SOY * 2)
    bgRect:setFillColor(70, 70, 70, 255)
    lg:insert(bgRect)

    if data then
        levelData = data
    else
        levelData = {}
        levelData.bombGrid = {}
        levelData.maxTouches = 3
        for i = 1, 30 do
            levelData.bombGrid[i] = 0
        end
    end

    local touchSlider = newSlider(9, levelData.maxTouches, function(value)
        levelData.maxTouches = value
    end)
    lg:insert(touchSlider)

    local ww = 1 -- world index
    local ll = 1 -- level index

    local worldSlider = newSlider(4, 1, function(value)
        ww = value
    end)
    lg:insert(worldSlider)
    worldSlider:setReferencePoint(display.BottomLeftReferencePoint)
    worldSlider.x = SOX
    worldSlider.y = CH - SOY - 40

    local levelSlider = newSlider(100, 1, function(value)
        ll = value
    end)
    lg:insert(levelSlider)
    levelSlider:setReferencePoint(display.BottomRightReferencePoint)
    levelSlider.x = CW - SOX
    levelSlider.y = CH - SOY

    local blocks = createBlocks(levelData.bombGrid)
    lg:insert(blocks)

    -- PLAY

    local playBut = display.newImage("images/play.png")
    playBut.x = display.contentWidth / 2
    playBut.y = 20

    function playBut:touch(e)
        if e.phase == "began" then
            director:changeScene("test", levelData)
        end
        return true
    end
    playBut:addEventListener("touch", playBut)

    -- SAVE

    local saveBut = display.newImage("images/save.png")
    saveBut.x = display.contentWidth / 2 - 70
    saveBut.y = 20

    function saveBut:touch(e)
        if e.phase == "began" then
            local serverUrl
            if system.getInfo("platformName") == "Android" then
                serverUrl = "http://spider-player.com/game/save.php?data="
            else
                serverUrl = "http://localhost/bomb/save.php?data=%s&filename=%s"
            end
            local data = json.encode(levelData)
            local fileName = string.format("%02.f_%04.f", ww, ll)
            network.request(string.format(serverUrl, url.escape(data), fileName),
                "GET",
                function(e)
                    if not e.isError then
                        print("Level successfully stored on the server.")
--                        native.showAlert("Done!", "Level successfully stored on the server.",
--                            {"OK, cool!"})
                    else
                        print("Couldn't save the level on remote server.")
                        native.showAlert("Fail!", "Couldn't save the level on remote server.",
                            {"Damn!"})
                    end
                end
            )
        end
        return true
    end
    saveBut:addEventListener("touch", saveBut)

    -- CLEAR

    local clearBut = display.newImage("images/clear.png")
    clearBut.x = CW / 2 + 70
    clearBut.y = 20

    function clearBut:touch(e)
        if e.phase == "began" then
            resetBlocks(blocks)
        end
        return true
    end
    clearBut:addEventListener("touch", clearBut)

    -- BACK

    local backBut = screen.newScaledImage("back")
    backBut.x = SOX + 30
    backBut.y = SOY + 50

    function backBut:touch(e)
        if e.phase == "began" then
            director:changeScene("mainmenu")
        end
        return true
    end
    backBut:addEventListener("touch", backBut)

    --------------------------

    lg:insert(playBut)
    lg:insert(saveBut)
    lg:insert(clearBut)
    lg:insert(backBut)

    return lg
end