module(..., package.seeall)

require "physics"
require "io"
local json = require "json"
require "gameNetwork"

require "counter"
require "bomb"
require "bullet"
require "sound"
require "settings"

--require "inneractive"
--require "admob"

-----------------------------------------------------------------

-- Abbreviations

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
--local CSX = display.contentScaleX
--local CSY = display.contentScaleY

local CSX = screen.getCSX()
local CSY = CSX

-----------------------------------------------------------------

-- Utility functions

local function map(v, x, y, nx, ny)
    return nx + (ny - nx) * (v - x) / (y - x)
end

local function round(n)
    return math.floor(n + 0.5)
end

local function createStopwatch()
    local watch = {}

    function watch:start()
        self.pauseTime = 0
        self.startTime = system.getTimer()
    end

    function watch:stop()
        -- if paused, resume first
        self:resume()
        if self.startTime then
            self.stopTime = system.getTimer()
            self.playTime = self.stopTime - self.startTime - self.pauseTime
            self.startTime = nil
            self.stopTime = nil
        end
    end

    function watch:pause()
        if self.startTime and not self.pauseStart then
            self.pauseStart = system.getTimer()
        end
    end

    function watch:resume()
        if self.pauseStart then
            self.pauseEnd = system.getTimer()
            self.pauseTime = self.pauseTime + (self.pauseEnd - self.pauseStart)
            self.pauseStart = nil
            self.pauseEnd = nil
        end
    end

    return watch
end

-----------------------------------------------------------------

local worldIndex
local levelIndex
local stopwatch = createStopwatch()

local function createDoors()
    local lg = display.newGroup()

    local zero = 0.001

    local lDoor = screen.newScaledImage("door_left")
    lDoor:setReferencePoint(display.CenterLeftReferencePoint)
    lDoor.x = SOX
    lDoor.y = CH / 2
    lDoor:scale(1, display.contentScaleY * screen.height / lDoor.contentHeight)

    local rDoor = screen.newScaledImage("door_right")
    rDoor:setReferencePoint(display.CenterRightReferencePoint)
    rDoor.x = CW - SOX
    rDoor.y = CH / 2
    rDoor:scale(1, display.contentScaleY * screen.height / rDoor.contentHeight)

    local lEdge = screen.newScaledImage("edge_left")
    lEdge:setReferencePoint(display.CenterLeftReferencePoint)
    lEdge.x = CW / 2
    lEdge.y = CH / 2
    lEdge:scale(zero, display.contentScaleY * screen.height / lEdge.contentHeight)

    local rEdge = screen.newScaledImage("edge_right")
    rEdge:setReferencePoint(display.CenterRightReferencePoint)
    rEdge.x = CW / 2
    rEdge.y = CH / 2
    rEdge:scale(zero, display.contentScaleY * screen.height / rEdge.contentHeight)

    function lg:openDoors(time, onComplete)
        time = time or 1000

        local count = 0
        local function done()
            count = count + 1
            if count == 6 then
                self.isVisible = false
                if onComplete then onComplete() end
            end
        end

        transition.to(lDoor, {time=time, xScale=zero, x=lDoor.x-20, onComplete=done})
        transition.to(rDoor, {time=time, xScale=zero, x=rDoor.x+20, onComplete=done})
        transition.to(lEdge, {time=time, x=SOX-20, onComplete=done})
        transition.to(rEdge, {time=time, x=CW-SOX+20, onComplete=done})
        transition.to(lEdge, {time=time, xScale=0.5, transition=easing.outExpo, onComplete=done})
        transition.to(rEdge, {time=time, xScale=0.5, transition=easing.outExpo, onComplete=done})
    end

    function lg:closeDoors(time, onComplete)
        time = time or 1000

        local count = 0
        local function done()
            count = count + 1
            if count == 6 and onComplete then
                onComplete()
            end
        end

        self.isVisible = true

        transition.to(lDoor, {time=time, xScale=CSX, x=SOX, onComplete=done})
        transition.to(rDoor, {time=time, xScale=CSX, x=CW-SOX, onComplete=done})
        transition.to(lEdge, {time=time, x=CW/2, onComplete=done})
        transition.to(rEdge, {time=time, x=CW/2, onComplete=done})
        transition.to(lEdge, {time=time, xScale=zero, transition=easing.inExpo, onComplete=done})
        transition.to(rEdge, {time=time, xScale=zero, transition=easing.inExpo, onComplete=done})
    end

    lg:insert(lEdge)
    lg:insert(rEdge)
    lg:insert(lDoor)
    lg:insert(rDoor)

    return lg
end

local ssFlyRocket = screen.loadSpriteSheet("fly_rocket_ss")
for i = 1, ssFlyRocket.frameCount do
    ssFlyRocket:add(tostring(i), i, 1, 100, 1)
end

local function createRocketGroup()
    local lg = display.newGroup()

    local fired = 0

    local function newRocket()
        local num = math.random(1, 3)
        local minAngle, maxAngle = -15, 15
        local scale = 1 * math.random(60, 100) / 100
        local delta = 40

        local rocket = ssFlyRocket:newSprite()
        rocket:prepare(tostring(num))
        rocket:setReferencePoint(display.BottomCenterReferencePoint)
        rocket.rotation = math.random(minAngle, maxAngle)
        local rx = map(rocket.rotation, minAngle, maxAngle, SOX - delta, CW - SOX + delta)
        rx = CW - rx
        rocket.x = CW / 2
        rocket.y = CH - SOY + rocket.contentHeight + 10
        rocket:scale(scale, scale)

        function rocket:clean()
            transition.cancel(self.fxTween)
        end

        local function removeRocket()
            rocket:removeSelf()
        end

        local distance = CH * 2
        local angle = rocket.rotation * math.pi / 180
        local toX = rocket.x + distance * math.sin(angle)
        local toY = rocket.y - distance * math.cos(angle)

        rocket.fxTween = transition.to(rocket, {time=1500, x=toX, y=toY,
            transition=easing.outQuad, onComplete=removeRocket})

        sound.play("rocket")

        return rocket
    end

    function lg:clean()
        timer.cancel(self.fxTimer)
    end

    local function randomShoot()
        if fired < 6 then
            lg.fxTimer = timer.performWithDelay(math.random(200, 400), randomShoot)
            fired = fired + 1
            lg:insert(newRocket())
--        else
--            lg.fxTimer = timer.performWithDelay(500, function()
--                lg:insert(newRocket())
--            end)
        end
    end

    randomShoot()
    sound.play("applause")

    return lg
end

-----------------------------------------------------------------

-- Navigation logic

--local function isLastRemainingLevel()
--    local levels = settings.data.worlds[worldIndex].levels
--    -- check if this is the only uncompleted level left in the world
--    for i = 1, #levels do
--        if i ~= levelIndex and levels[i] <= 0 then
--            return false
--        end
--    end
--    return true
--end
--
--local function firstUncompletedLevel(startIndex, endIndex)
--    local levels = settings.data.worlds[worldIndex].levels
--    for i = startIndex, endIndex do
--        if levels[i] <= 0 then
--            return i
--        end
--    end
--    return 0
--end

local function selectLevel()
    local data = settings.data
    local world = data.worlds[worldIndex]
    if data.gameScore > data.feintScore.game then
        data.feintScore.game = data.gameScore
        gameNetwork.request( "setHighScore", {
            leaderboardID = settings.feint.leaderboards.game,
            score = data.gameScore,
            displayText = tostring(data.gameScore)
        })
    end
    if world.score > data.feintScore.worlds[worldIndex] then
        data.feintScore.worlds[worldIndex] = world.score
        gameNetwork.request( "setHighScore", {
            leaderboardID = settings.feint.leaderboards.worlds[worldIndex],
            score = world.score,
            displayText = tostring(world.score)
        })
    end
    sound.play("click")
    director:changeScene("levelmenu", worldIndex)
end

local function restartLevel(open)
    director:changeScene(_NAME, worldIndex, levelIndex, open)
end

local function nextLevel(toTheNextWorld, openDoors)
    local worlds = settings.data.worlds

    -- world completed or last level reached
    if toTheNextWorld then
        if worldIndex < #worlds then
            -- go to the next world
            director:changeScene("worldmenu", worldIndex, true)
        else
            -- no more worlds
            native.showAlert("Thank you for playing!", "You have finished the game!\n\n" ..
                "Check back for the new levels coming with the updates.", {"Close"})
        end
    else
        local levelCount = #worlds[worldIndex].levels
        if levelIndex < levelCount then
            director:changeScene(_NAME, worldIndex, levelIndex + 1, openDoors)
        else
            sound.play("click")
            director:changeScene("levelmenu", worldIndex)
        end
    end

end

local function skipLevel()
    nextLevel(false, false)
end

local function saveDataAndUnlockNextLevel()
    local data = settings.data
    local world = data.worlds[worldIndex]
    local levels = world.levels
    local oldScore = levels[levelIndex]

    local minTime, maxTime = 1000, 30000
    local minScore, maxScore = 100, 999
    local time = stopwatch.playTime

    -- score time is used for score calculation and can't be less than 1 sec. and bigger than 30 sec.
    local scoreTime = time
    if scoreTime < minTime then
        scoreTime = minTime
    elseif scoreTime > maxTime then
        scoreTime = maxTime
    end

    -- calculate score
    local score = round(minScore + maxScore - map(scoreTime, minTime, maxTime, minScore, maxScore)) * 10

    -- save new score, if bigger
    if score > oldScore then
        -- is level completed for the first time?
        if oldScore == 0 then
            world.progress = world.progress + 1
            data.picklocks = data.picklocks + 1
        end
        levels[levelIndex] = score
        world.score = world.score - oldScore + score
        data.gameScore = data.gameScore - oldScore + score
    end

    -- unlock next level if exists and locked
    if levelIndex < #levels then
        if levels[levelIndex+1] < 0 then
            levels[levelIndex+1] = 0
        end
    end
end

-----------------------------------------------------------------

local function createPauseMenu()
    local menuBusy = false
    local paused = false
    local effectTime = 333

    local lg = display.newGroup()

    local slidePanel = display.newGroup()

    local tintRect = screen.newFullRect(true)

    local xOffscreen = SOX - 150

    local refresh = screen.newTouchImage("refresh", function()
        if not menuBusy then
            restartLevel(false)
        end
    end)
    refresh.x = SOX + 48
    refresh.y = SOY + 50

    local skip = screen.newTouchImage("skip", function()
        if not menuBusy then
            skipLevel()
        end
    end)
    skip.x = SOX + 48
    skip.y = SOY + 110

    local list = screen.newButton{
        default = "list",
        onTap = function()
            if not menuBusy then
                selectLevel()
            end
        end
    }
    list.x = SOX + 44
    list.y = CH - SOY - 50

    local mute = screen.newToggle{
        defaultOn = "sound_on",
        defaultOff = "sound_off",
        defaultScale = 0.8,
        overScale = 1,
        isOn = settings.data.soundsOn,
        onTap = function(isOn)
            settings.data.soundsOn = isOn
            sound.play("click")
        end
    }
    mute:setReferencePoint(display.CenterReferencePoint)
    mute.x = SOX + 48
    mute.y = skip.y + (list.y - skip.y) / 2

    local function toggleMenu()
        if not paused then
            lg:showMenu()
        else
            lg:hideMenu()
        end
    end

    local pause = screen.newTouchImage("pause", toggleMenu)
    pause:setReferencePoint(display.TopRightReferencePoint)
    pause.x = CW - 10
    pause.y = SOY + 10

    function lg:blockMenu(block)
        menuBusy = block
    end

    function lg:showMenu()
        if not menuBusy then
            stopwatch:pause()
            physics.pause()
            menuBusy = true
            paused = true
            transition.to(tintRect, {time = effectTime, alpha = 0.5})
            transition.to(slidePanel, {time = effectTime, x = SOX, onComplete=function()
                menuBusy = false
            end})
        end
    end

    function lg:hideMenu()
        if not menuBusy then
            menuBusy = true
            paused = false
            transition.to(slidePanel, {time = effectTime, x = xOffscreen})
            transition.to(tintRect, {time = effectTime, alpha = 0, onComplete=function()
                physics.start()
                stopwatch:resume()
                menuBusy = false
            end})
        end
    end

    local curtain = screen.newTouchImage("curtain", function()
        lg:hideMenu()
    end)
    curtain:setReferencePoint(display.CenterLeftReferencePoint)
    curtain.x = SOX
    curtain.y = CH / 2
    curtain:scale(1, display.contentScaleY * screen.height / curtain.contentHeight)

    slidePanel:insert(curtain)
    slidePanel:insert(refresh)
    slidePanel:insert(skip)
    slidePanel:insert(mute)
    slidePanel:insert(list)

    lg:insert(tintRect)
    lg:insert(pause)
    lg:insert(slidePanel)

    slidePanel.x = xOffscreen

    local function onKeyEvent(e)
        local returnValue = false
        if e.phase == "down" then
            if "menu" == e.keyName or "back" == e.keyName then
                returnValue = true
                toggleMenu()
            end
        end
        return returnValue
    end

    function lg:clean()
        Runtime:removeEventListener("key", onKeyEvent)
    end

    Runtime:addEventListener("key", onKeyEvent)

    return lg
end

local function createLamp(parent)
    local lg = display.newGroup()

    local lightAlpha = 0.7
    local closetAlpha = 0.01

    local lamp = screen.newScaledImage("lamp")
    lamp:setReferencePoint(display.TopCenterReferencePoint)
    lamp.x = CW / 2
    lamp.y = SOY - 5
    lg:insert(lamp)

    local light = screen.newScaledImage("light")
    light:setReferencePoint(display.TopCenterReferencePoint)
    light.x = CW / 2
    light.y = lamp.y + 62
    light.alpha = lightAlpha
    lg:insert(light)
    lamp:toFront()

    local dead = screen.newScaledImage("lamp_dead")
    dead:setReferencePoint(display.TopCenterReferencePoint)
    dead.x = lamp.x
    dead.y = lamp.y
    dead.isVisible = false
    lg:insert(dead)

    function light:cancelTimer()
        if self.flickTimer then
            timer.cancel(self.flickTimer)
        end
    end

    function light:clean()
        self:cancelTimer()
    end

    local closet = screen.newScaledImage("closet_bg")
    closet:setReferencePoint(display.CenterReferencePoint)
    closet.x = CW / 2
    -- 18px is a screenOriginY difference between 800 and 854 when base resolution is 320x480
    closet.y = CH / 2 + (screen.height == 800 and -18 or 0)
    parent:insert(closet)
    closet:toBack()
    closet.alpha = closetAlpha

    function light:flicker(minTime, maxTime)
        local this = self
        local max = 10
        local n = 0
        local flick, unflick

        flick = function()
            n = n + 1
            this.alpha = 0
            closet.alpha = closetAlpha
            this.flickTimer = timer.performWithDelay(33, unflick)
        end

        unflick = function()
            if n < max then
                this.alpha = 1
                closet.alpha = 1
                this.flickTimer = timer.performWithDelay(math.random(50, 250), flick)
            else
                this.alpha = lightAlpha
                closet.alpha = closetAlpha
                this:flicker(7000, 20000)
            end
        end

        this.flickTimer = timer.performWithDelay(math.random(minTime, maxTime), function()
            flick()
        end)
    end

    function lg:clean()
        if self.flashTimer then
            timer.cancel(self.flashTimer)
        end
        if self.bulbTimer then
            timer.cancel(self.bulbTimer)
        end
        if self.awwTimer then
            timer.cancel(self.awwTimer)
        end
    end

    function lg:popBulb(parent, onPop)
        light:cancelTimer()
        sound.play( "bulb" .. tostring(math.random(1, 2)) )
        -- delay the fx to account for the audio playback lag
        local fxDelay = settings.isDevice and 200 or 70
        self.flashTimer = timer.performWithDelay(fxDelay - 70, function()
            light.alpha = 1
            closet.alpha = 1
        end)
        self.bulbTimer = timer.performWithDelay(fxDelay, function()
            closet.isVisible = false
            lamp.isVisible = false
            light.isVisible = false
            dead.isVisible = true
            onPop()

            local rect = display.newRect(0, 0, 10, 10)
            rect:setReferencePoint(display.TopCenterReferencePoint)
            rect.x = CW / 2
            rect.y = SOY
            rect.isVisible = false
            parent:insert(rect)
            parent:setReferencePoint(display.TopCenterReferencePoint)
            parent.rotation = lg.rotation

            bomb.createRays(parent, dead.x, dead.y + 72)
        end)
        self.awwTimer = timer.performWithDelay(fxDelay + 500, function()
            sound.play("aww")
        end)
    end

    lg:setReferencePoint(display.TopCenterReferencePoint)
    lg.rotation = 40

    local swingForward, swingBack

    swingForward = function()
        transition.to(lg, {time=3000, rotation=-20, transition=easing.inOutQuad, onComplete=swingBack})
    end

    swingBack = function()
        transition.to(lg, {time=3000, rotation=20, transition=easing.inOutQuad, onComplete=swingForward})
    end

    swingForward()
    light:flicker(3000, 15000)

    return lg
end

local function createWalls()
    local thickness = 20
    local name = "wall"

    local height = CH - SOY * 2
    local width  = CW - SOX * 2

    local lg = display.newGroup()

    local leftWall = display.newRect(SOX - thickness, SOY, thickness, height)
    local rightWall = display.newRect(CW - SOX, SOY, thickness, height)
    local topWall = display.newRect(SOX, SOY - thickness, width, thickness)
    local bottomWall = display.newRect(SOX, CH - SOY, width, thickness)

    leftWall.name = name
    rightWall.name = name
    topWall.name = name
    bottomWall.name = name

    physics.addBody(leftWall, "static")
    physics.addBody(rightWall, "static")
    physics.addBody(topWall, "static")
    physics.addBody(bottomWall, "static")

    lg:insert(leftWall)
    lg:insert(rightWall)
    lg:insert(topWall)
    lg:insert(bottomWall)

    return lg
end

local ssPlate = screen.loadSpriteSheet("plate_ss")
for i = 1, ssPlate.frameCount do
    ssPlate:add(tostring(i), i, 1, 100, 1)
end

local function createBombs(grid)
    local lg = display.newGroup()

    local xBlocks = 5
    local yBlocks = 6

    local blockWidth  = CW  / xBlocks
    local blockHeight = blockWidth
    local yShift = screen.imageSuffix == "_480" and 18 or 30

    local cx, cy, block, blockIndex

    local function canTouch()
        return counter.getTouches() > 0
    end

    local shelfEdge = screen.newScaledImage("shelf_edge")
    shelfEdge:setReferencePoint(display.BottomLeftReferencePoint)
    shelfEdge.x = 0
    shelfEdge.y = blockHeight / 2 + 9 + yShift
    lg:insert(shelfEdge)

    for i = 1, yBlocks do
        for j = 1, xBlocks do
            cx = blockWidth * j - blockWidth / 2
            cy = blockHeight * i - blockHeight / 2

            blockIndex = (i - 1) * xBlocks + j
            block = grid[ blockIndex ]

            if block > 0 then
                local b = bomb.new{
                    x = cx,
                    y = cy + (CH - blockHeight * 6) / 2 + yShift,
                    state = block,
                    canTouch = canTouch
                }
                lg:insert(b)
                b:toBack()
            end
        end

        local shelf = display.newImageRect("images/shelf.png", 320, 80)
        shelf:setReferencePoint(display.TopLeftReferencePoint)
        shelf.x = 0
        shelf.y = blockHeight * i - blockHeight / 2 + 8 + yShift
        lg:insert(shelf)

        for k = 1, 4 do
            local plate = ssPlate:newSprite()
            plate:prepare(tostring(math.random(1, 3)))
            plate.x = CW / 5 * k
            plate.y =  shelf.y + 67
            lg:insert(plate)
        end

        shelf:toBack()
    end

    local frameStart = blockHeight * yBlocks + 46
    local frameEnd = CH - SOY
    for i = 1, math.floor((frameEnd - frameStart) / 6 + 0.5) + 1 do
        local shelfFrame = screen.newScaledImage("shelf_frame")
        shelfFrame:setReferencePoint(display.BottomLeftReferencePoint)
        shelfFrame.x = 0
        shelfFrame.y = i * 6 + frameStart + yShift
        lg:insert(shelfFrame)
        shelfFrame:toBack()
    end

    return lg
end

local function createTouchMeter()
    local lg = display.newGroup()

    local updateText = screen.updateText

    local handIcon = screen.newScaledImage("touch")
    handIcon:setReferencePoint(display.TopLeftReferencePoint)
    handIcon.x = 10
    handIcon.y = SOY + 10
    lg:insert(handIcon)

    local text = screen.newScaledText("", 0, 0, settings.mainFont, 21)
    lg:insert(text)

    local function onTouch(left, made, max)
        updateText(text, tostring(left), 32, SOY + 4)
    end

    function lg:updateMeter()
        onTouch(counter.getTouches())
    end

    counter.onTouch = onTouch
    lg:updateMeter()

    return lg
end

local function showLevelNumber(parentGroup)
    local textY, fontSize

    if screen.height / screen.width > 1.65 then
        textY = CH - SOY - 48 * screen.width / screen.height
        fontSize = 24
    else
        textY = SOY + 18
        fontSize = 21
    end

    local text = screen.newScaledText(string.format("Level %s-%s", worldIndex, levelIndex),
        0, 0, settings.mainFont, fontSize)
    text:setReferencePoint(display.CenterReferencePoint)
    text.x = CW / 2
    text.y = textY
    text.alpha = 0

    function text:clean()
        transition.cancel(self.fxTween)
    end

    local function removeText()
        text:removeSelf()
    end

    local function hideText()
        text.fxTween = transition.to(text, {time=666, delay=1000, alpha=0, onComplete=removeText})
    end

    local function showText()
        text.fxTween = transition.to(text, {time=666, delay=500, alpha=1, onComplete=hideText})
    end

    parentGroup:insert(text)
    showText()
end

local function pleaseRate()
    local data = settings.data

    if data.rateAlerts == -1 then return end


    if (data.rateAlerts == 0 and worldIndex == 1 and levelIndex == 10) or
       (data.rateAlerts == 1 and worldIndex == 1 and levelIndex == 50) or
       (data.rateAlerts == 2 and worldIndex == 1 and levelIndex == 100) then

        local function onRateAlert(e)
            if "clicked" == e.action then
                if e.index == 1 then
                    data.rateAlerts = data.rateAlerts + 1 -- ask later
                    if data.rateAlerts >= 3 then
                        data.rateAlerts = -1 -- don't ask more than three times
                    end
                elseif e.index == 2 then
                    data.rateAlerts = -1 -- player agreed to rate, don't ask again
                    settings.openMarketPage()
                elseif e.index == 3 then
                    data.rateAlerts = -1 -- player doesn't want to rate, don't ask again
                end
            end
        end

        native.showAlert("WELL DONE!",
            "Do you like this game?\n\n" ..
            "If you do, please rate it in the Marketplace.\n\n" ..
            "Good ratings will encourage us to update Pyro Shelf on a regular basis.\n\n" ..
            "Thank you!",
            {"Rate later", "Rate Now", "No, thanks"}, onRateAlert)
    end
end

local function createPopup()
    local lg = display.newGroup()
    local popup = display.newGroup()
    local tintRect = screen.newFullRect(true)

    local xShift = 55
    local yShift = -8
    local yDelta = 64
    local ref = display.CenterReferencePoint

    local successPlate = screen.newScaledImage("success")
    successPlate.x = CW / 2
    successPlate.y = 0

    popup:insert(successPlate)

    ---------------------------------------------------------------------------

    local levelPlate = screen.newScaledImage("plate")
    levelPlate.x = CW / 2
    levelPlate.y = yDelta

    local levelText = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    screen.updateText(levelText, string.format("Level %s-%s", worldIndex, levelIndex),
        levelPlate.x, levelPlate.y, ref)
    levelText:setTextColor(0, 0, 0)

    popup:insert(levelPlate)
    popup:insert(levelText)

    ---------------------------------------------------------------------------

    local timePlate = screen.newScaledImage("plate")
    timePlate.x = CW / 2
    timePlate.y = yDelta * 2

    local timeText = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    screen.updateText(timeText, "Time: 00:00", timePlate.x, timePlate.y, ref)
    timeText:setTextColor(0, 0, 0)

    popup:insert(timePlate)
    popup:insert(timeText)

    ---------------------------------------------------------------------------

    local scorePlate = screen.newScaledImage("plate")
    scorePlate.x = CW / 2
    scorePlate.y = yDelta * 3

    local scoreText = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    screen.updateText(scoreText, "Score: 0000", scorePlate.x, scorePlate.y, ref)
    scoreText:setTextColor(0, 0, 0)

    popup:insert(scorePlate)
    popup:insert(scoreText)

    ---------------------------------------------------------------------------

    local buttonPlate = screen.newScaledImage("button_plate")
    buttonPlate.x = CW / 2
    buttonPlate.y = yDelta * 4
    popup:insert(buttonPlate)

    local list = screen.newButton{
        default = "list",
        onTap = selectLevel
    }
    list.x = CW / 2 - xShift
    list.y = yDelta * 4
    popup:insert(list)

    ---------------------------------------------------------------------------

    local refresh = screen.newTouchImage("refresh", function()
--        inneractive.hideAd()
--        admob.hideAd()
        restartLevel(true)
    end)
    refresh.x = CW / 2
    refresh.y = yDelta * 4
    popup:insert(refresh)

    ---------------------------------------------------------------------------

    local world = settings.data.worlds[worldIndex]
    local oldProgress = world.progress
    local next = screen.newTouchImage("next", function()
        local levelCount = #world.levels
        local toTheNextWorld = (oldProgress < levelCount and world.progress == levelCount)
            or (levelIndex == levelCount and world.progress == levelCount)
--        inneractive.hideAd()
--        admob.hideAd()
        nextLevel(toTheNextWorld, true)
    end)
    next.x = CW / 2 + xShift
    next.y = yDelta * 4

    local function nextZoomIn()
        local function nextZoomOut()
            transition.to(next, {time=500, xScale=0.9*CSX, yScale=0.9*CSY, onComplete=nextZoomIn})
        end
        transition.to(next, {time=500, xScale=1.1*CSX, yScale=1.1*CSY, onComplete=nextZoomOut})
    end

    popup:insert(next)

    ---------------------------------------------------------------------------

    local improved = screen.newScaledImage("improved")
    improved:setReferencePoint(ref)
    improved.x = CW - SOX - 80
    improved.y = 40
    improved:scale(CSX * 3, CSY * 3)
    improved.alpha = 0

    ---------------------------------------------------------------------------

    local oldScore = settings.data.worlds[worldIndex].levels[levelIndex]

    function lg:updateTimeAndScore()
        local time = stopwatch.playTime
        local minutes = math.floor( time / 60000 )
        local seconds = round( math.fmod(time, 60000) / 1000 )
        local score = settings.data.worlds[worldIndex].levels[levelIndex]

        if score > oldScore and oldScore > 0 then
            self.improved = true
        end

        screen.updateText(timeText, string.format("Time: %02.f:%02.f", minutes, seconds),
            timePlate.x, timePlate.y, ref)
        screen.updateText(scoreText, string.format("Score: %04.f", score),
            scorePlate.x, scorePlate.y, ref)
    end

    function lg:showPopup()
        pleaseRate()
        popup.isVisible = true
        if self.improved then
            transition.to(improved, {time=500, delay=1200, xScale=CSX, yScale=CSY, alpha=1})
--            transition.to(improved, {time=500, delay=2500, xScale=CSX, yScale=CSY, alpha=1})
        end
        transition.to(popup, {time=1000, delay=500, y=CH/2, transition=easing.outExpo})
--        transition.to(popup, {time=1500, delay=1500, y=CH/2, transition=easing.outExpo})
        if worldIndex == 1 and levelIndex == 1 then
            nextZoomIn()
        end
    end

    popup:setReferencePoint(ref)
    popup.y = SOY - popup.contentHeight
    popup.isVisible = false

    lg:insert(tintRect)
    lg:insert(popup)
    lg:insert(improved)

    return lg
end

local function loadLevelData(path)
    path = system.pathForFile(path, system.ResourceDirectory)
    local file = io.open(path, "r")
    if file then
        local contents = file:read("*a")
        io.close(file)
        if contents and contents ~= "" then
--            print("Level data successfully loaded:", path)
            return json.decode(contents)
        end
    else
        print(string.format("Can't open file %s", path))
    end
end

function new(world, level, open)
    worldIndex = world
    levelIndex = level

    physics.start()
--    physics.setDrawMode("hybrid")
    physics.setGravity(0, 0)

    -- filename in the format of "xx.xxxx.txt"
    local fileName = string.format("%02.f_%04.f.txt", worldIndex, levelIndex)

    local levelData = loadLevelData(fileName)

    if levelData then
        -- unlock this level, if locked
        local levels = settings.data.worlds[worldIndex].levels
        if levels[levelIndex] < 0 then
            levels[levelIndex] = 0
        end
    end

    bomb.onEvent = counter.countBombs
    bullet.onEvent = counter.countBullets
    counter.reset(levelData.maxTouches)

    -- creating a local group to put everything into and return
    local lg = display.newGroup()

    lg:insert(createWalls())

    local bombGroup = createBombs(levelData.bombGrid)
    lg:insert(bombGroup)

    local touchMeter = createTouchMeter()
    lg:insert(touchMeter)

    local lampGroup = createLamp(lg)
    lg:insert(lampGroup)

    local pauseMenu = createPauseMenu()
    lg:insert(pauseMenu)

    function lg:clean()
        if self.doorTimer then
            timer.cancel(self.doorTimer)
        end
        if self.popTimer then
            timer.cancel(self.popTimer)
        end
    end

    local doors = createDoors()
    lg:insert(doors)

    local lockRect = screen.newFullRect(true)
    lg:insert(lockRect)

    -- note: popup is invisible upon creation
    local popup = createPopup()
    lg:insert(popup)

    counter.onVictory = function()
        stopwatch:stop()
        saveDataAndUnlockNextLevel()

--        timer.performWithDelay(35, function()
--            inneractive.showAd(1, SOX, CH - SOY - 53, true)
--            admob.showAd(false)
--        end)

        lg:insert(createRocketGroup())

        lg:insert(doors)
        lg:insert(lockRect)
        lockRect.isHitTestable = true
        lg:insert(popup)
        popup:updateTimeAndScore()
        lg.doorTimer = timer.performWithDelay(1500, function()
--        lg.doorTimer = timer.performWithDelay(2600, function()
            popup:showPopup()
            doors:closeDoors()
        end)
    end

    local function onPop()
        lockRect.alpha = 0.5
        transition.to(lockRect, {time=1500, delay=1000, alpha=1,
            transition=easing.inQuad, onComplete=function() restartLevel(false) end}
        )
    end

    counter.onDefeat = function()
        lg.popTimer = timer.performWithDelay(400, function()
            pauseMenu:blockMenu(true)
            local bulbGroup = display.newGroup()
            lg:insert(bulbGroup)
            lampGroup:popBulb(bulbGroup, onPop)
        end)
    end

    showLevelNumber(lg)

    if open then
        lockRect.isHitTestable = true
        doors:openDoors(1000, function()
            lockRect.isHitTestable = false
            stopwatch:start()
        end)
    else
        doors:openDoors(0)
        lg.alpha = 0
        transition.to(lg, {time=666, delay=35, alpha=1, onComplete=function()
            stopwatch:start()
        end})
    end

    return lg
end

function clean()
    stopwatch:stop()
    counter.onTouch = nil
    counter.onVictory = nil
    counter.onDefeat = nil
    bomb.onEvent = nil
    bullet.onEvent = nil
end