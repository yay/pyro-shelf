module(..., package.seeall)


local bombTouches, maxBombTouches
local bombsCreated, bombsDestroyed
local bulletsCreated, bulletsDestroyed

local won, lost

onVictory = nil
onDefeat = nil
onTouch = nil

function reset(maxTouches)
    won = false
    lost = false
    bombsCreated = 0
    bombsDestroyed = 0
    bombTouches = 0
    bulletsCreated = 0
    bulletsDestroyed = 0
    maxBombTouches = maxTouches
end

-- returns number of touches: left, made, max
function getTouches()
    return maxBombTouches - bombTouches, bombTouches, maxBombTouches
end

local function printStats()
    print("Bullets created:   ", bulletsCreated)
    print("Bullets destroyed: ", bulletsDestroyed)
    print("Bombs created:   ", bombsCreated)
    print("Bombs destroyed: ", bombsDestroyed)
    print("maxBombTouches:  ", maxBombTouches)
    print("bombTouches:     ", bombTouches)
    print("-------------------------------")
end

local function declareVictory()
    if not won and not lost then
        won = true
        if type(onVictory) == "function" then
            onVictory()
        end
    end
end

local function declareDefeat()
    if not won and not lost then
        lost = true
        if type(onDefeat) == "function" then
            onDefeat()
        end
    end
end

function countBombs(e)
    if e.name == "create" then
        bombsCreated = bombsCreated + 1
    elseif e.name == "destroy" then
        bombsDestroyed = bombsDestroyed + 1
    elseif e.name == "touch" then
        bombTouches = bombTouches + 1
        if type(onTouch) == "function" then
            onTouch(getTouches())
        end
    end

--    printStats()

    if bombTouches > maxBombTouches or
        (bombTouches == maxBombTouches and bulletsCreated == bulletsDestroyed) then
        declareDefeat()
    end

    if bombsDestroyed == bombsCreated then
        declareVictory()
    end
end

function countBullets(e)
    if e.name == "create" then
        bulletsCreated = bulletsCreated + 1
    elseif e.name == "destroy" then
        bulletsDestroyed = bulletsDestroyed + 1
    end

--    printStats()

    if (bulletsDestroyed == bulletsCreated) and
       (bombsDestroyed < bombsCreated) and
       (bombTouches >= maxBombTouches) then
        declareDefeat()
    end
end