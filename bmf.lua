module(..., package.seeall)

local sprite = require "sprite"

function loadFont(fntFile)
    local function extract(s, p)
        return string.match(s, p), string.gsub(s, p, '', 1)
    end

    local font = { info = {}, spritesheets = {}, sprites = {}, chars = {}, kernings = {} }

    local readline = io.lines(system.pathForFile(fntFile, system.ResourceDirectory))
    for line in readline do
        local t = {}
        local tag
        tag, line = extract( line, '^%s*([%a_]+)%s*' )
        while string.len( line ) > 0 do
            local k, v
            k, line = extract( line, '^([%a_]+)=' )
            if not k then break end
            v, line = extract( line, '^"([^"]*)"%s*' )
            if not v then
                v, line = extract( line, '^([^%s]*)%s*' )
            end
            if not v then break end
            t[ k ] = v
        end
        if tag == "info" or tag == "common" then
            for k, v in pairs(t) do font.info[ k ] = v end
        elseif tag == "page" then
            font.spritesheets[ 1 + t.id ] = { file = t.file, frames = {} }
        elseif tag == "char" then
            t.letter = string.char( t.id )
            font.chars[ t.letter ] = {}
            for k, v in pairs( t ) do font.chars[ t.letter ][ k ] = v end
            if 0 + font.chars[ t.letter ].width > 0 and 0 + font.chars[ t.letter ].height > 0 then
                font.spritesheets[ 1 + t.page ].frames[ #font.spritesheets[ 1 + t.page ].frames + 1 ] = {
                    textureRect = { x = 0 + t.x, y = 0 + t.y, width = t.width, height = t.height },
                    spriteSourceSize = { width = 0 + t.width, height = 0 + t.height },
                    spriteColorRect = { x = 0, y = 0, width = -1 + t.width, height = -1 + t.height },
                    spriteTrimmed = true
                }
                font.sprites[ t.letter ] = {
                    spritesheet = 1 + t.page,
                    frame = #font.spritesheets[ 1 + t.page ].frames
                }
            end
        elseif tag == "kerning" then
            font.kernings[ string.char( t.first ) .. string.char( t.second ) ] = 0 + t.amount
        end
    end

    for k, v in pairs( font.spritesheets ) do
        font.spritesheets[ k ].sheet = sprite.newSpriteSheetFromData( v.file, v )
    end
    for k, v in pairs( font.sprites ) do
        font.sprites[ k ] = sprite.newSpriteSet( font.spritesheets[ v.spritesheet ].sheet, v.frame, 1 )
    end

    function font:dispose()
        for k in pairs( self.spritesheets ) do
            self.spritesheets[ k ].sheet:dispose()
        end
    end

    return font
end

function newString(sFont, sText)
    local obj = display.newGroup()

    local font, text

    function obj:getFont()
        return font
    end

    function obj:getText()
        return text
    end

    function obj:setText(t)
        if type(t) ~= "string" then return end

        text = t
        for i = self.numChildren, 1, -1 do self[i].removeSelf() end
        local x, y = 0, 0
        local last = ""

        for c in string.gmatch(text .. "\n", "(.)") do
            if c == "\n" then
                x = 0
                y = y + font.info.lineHeight
            elseif font.chars[c] then
                if 0 + font.chars[c].width > 0 and 0 + font.chars[c].height > 0 then
                    local letter = sprite.newSprite(font.sprites[c])
                    letter:setReferencePoint(display.TopLeftReferencePoint)
                    if font.kernings[last .. c] then
                        x = x + font.kernings[last .. c]
                    end
                    letter.x = font.chars[c].xoffset + x
                    letter.y = font.chars[c].yoffset - font.info.base + y
                    self:insert(letter)
                    last = c
                end
                x = x + font.chars[c].xadvance
            end
        end
    end

    function obj:setFont(f)
        font = f
        self:setText(text)
    end

    text = sText
    obj:setFont(sFont)

    return obj
end