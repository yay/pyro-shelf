module(..., package.seeall)

--====================================================================--
-- INFORMATION
--====================================================================--
--
-- * In main.lua file, import the class:
--
--   require "director"
--
-- * To change scenes, use this command:
--
--   director:changeScene("settings")
--
-- * Every scene is a lua module file and must have a new() function that
--   must return a local display group, like this:
--
--   module(..., package.seeall)
--   local localGroup = display.newGroup()
--   function new()
--     ------ Your code here ------
--     return localGroup
--   end
--
-- * Every display object must be inserted into the local display group
--
--   local background = display.newImage( "background.png" )
--   localGroup:insert( background )
--
-- * This class doesn't clean timers! If you want to stop timers when
--   changing scenes, you'll have to do it manually inside the clean() function.
--
--====================================================================--

local currentScene
local currentSceneName = "main"

------------------------------------------------------------------------
-- GET SCENES
------------------------------------------------------------------------

function director:getCurrentSceneName()
	return currentSceneName
end

------------------------------------------------------------------------
-- CLEAN GROUPS
------------------------------------------------------------------------

local coronaMetaTable = getmetatable(display.getCurrentStage())

local isDisplayObject = function(aDisplayObject)
	return (type(aDisplayObject) == "table" and getmetatable(aDisplayObject) == coronaMetaTable)
end

local function cleanGroups ( objectOrGroup, level )
	if(not isDisplayObject(objectOrGroup)) then return end

	if objectOrGroup.numChildren then
		while objectOrGroup.numChildren > 0 do
			cleanGroups ( objectOrGroup[objectOrGroup.numChildren], level+1 )
		end
    end

	if level > 0 then

		-- check if object/group has an attached touch listener
		if objectOrGroup.touch then
			objectOrGroup:removeEventListener( "touch", objectOrGroup )
			objectOrGroup.touch = nil
        end

		-- check to see if this object has any attached
		-- enterFrame listeners via object.enterFrame or
		-- object.repeatFunction
		if objectOrGroup.enterFrame then
			Runtime:removeEventListener( "enterFrame", objectOrGroup )
			objectOrGroup.enterFrame = nil
		end

		if objectOrGroup.repeatFunction then
			Runtime:removeEventListener( "enterFrame", objectOrGroup )
			objectOrGroup.repeatFunction = nil
		end

        if type(objectOrGroup["clean"]) == "function" then
            objectOrGroup:clean()
        end

		objectOrGroup:removeSelf()
	end
end

------------------------------------------------------------------------
-- CALL CLEAN FUNCTION
------------------------------------------------------------------------

local function callClean ( moduleName )
	if type(package.loaded[moduleName]) == "table" then
		if moduleName ~= "main" then
			for k,v in pairs(package.loaded[moduleName]) do
				if k == "clean" and type(v) == "function" then
					package.loaded[moduleName].clean()
				end
			end
		end
	end
end

------------------------------------------------------------------------
-- UNLOAD SCENE
------------------------------------------------------------------------

local function unloadScene ( moduleName )
	if moduleName ~= "main" and type(package.loaded[moduleName]) == "table" then
		package.loaded[moduleName] = nil
	    collectgarbage("collect")
	end
end

------------------------------------------------------------------------
-- CHANGE SCENE
------------------------------------------------------------------------

function director:changeScene(moduleName, ...)
    callClean  ( currentSceneName )
    cleanGroups( currentScene, 0  )

    if moduleName ~= currentSceneName then
        unloadScene( currentSceneName )
        currentScene = require(moduleName).new(...)
        currentSceneName = moduleName
    else
        currentScene = package.loaded[moduleName].new(...)
    end

    print("Memory in use:", tostring(collectgarbage("count")) .. " Kb")

	return true
end