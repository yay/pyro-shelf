module(..., package.seeall)

require "io"
local json = require "json"

isDevice = system.getInfo("environment") == "device"
isAmazonVersion = false

mainFont = isDevice and "Kontrapunkt Light" or "Candara"

local function listFonts()
    local fonts = native.getFontNames()
    for i, fontname in ipairs(fonts) do
        print("'" .. fontname .. "'");
    end
end

feint = {
    productKey = "oCxMUsfrVfo9hmjrydlzDw",
    productSecret = "GDLbE0XPJ1RxSDZEUVG716jMhjlBoNWqTMoq5GKW5Y",
    displayName = "Pyro Shelf",
    appId = "359033",
    leaderboards = {
        game = "906947",
        worlds = {
            "909007",
            "909017",
            "909027",
            "909037"
        }
    }
}

data = nil

local dataVersion = 2
local function resetData()
    data = {
        -- version of this data structure
        version = dataVersion,
        -- actual game score
        gameScore = 0,
        -- number of picklocks
        picklocks = 0,
        -- number of times player was asked to rate the game
        rateAlerts = 0,
        -- last submitted score
        feintScore = {
            game = 0,
            worlds = {}
        },
        worlds = {},
        soundsOn = true,
        currentWorld = 1,
        currentLevel = 1
    }
    for i = 1, 4 do
        local world = {}
        -- Set world's score:
        -- negative - world locked
        -- zero     - world unlocked
        -- positive - world's score
        world.score = i == 1 and 0 or -1
        world.progress = 0
        world.levels = {}
        for j = 1, 100 do
            -- Set level's score:
            -- negative - level locked
            -- zero     - level unlocked
            -- positive - level completed
            -- note: first level of each world is unlocked by default
            world.levels[j] = j == 1 and 0 or -1
        end
        data.worlds[i] = world
        data.feintScore.worlds[i] = 0
    end
end

resetData()

--print(json.encode(data))

local filename = "settings.json"

function save()
    print("Saving settings...")

    local path = system.pathForFile(filename, system.DocumentsDirectory)
    local file = io.open(path, "w+")
    if file then
        local contents = json.encode(data)
--        print(contents)
        file:write(contents)
        io.close(file)
    end
end

function load()
    print("Loading settings...")

    local path = system.pathForFile(filename, system.DocumentsDirectory)
    local file = io.open(path, "r")
    if file then
        local contents = file:read("*a")
        io.close(file)
        if contents and contents ~= "" then
--            print(contents)

            data = json.decode(contents)

            if data.version == 1 then
                data.picklocks = data.worlds[1].progress
            end

            data.version = dataVersion
        end
    else
    end
end

function openMarketPage()
    if isAmazonVersion then
        system.openURL("http://www.amazon.com/gp/mas/dl/android?p=pname:com.pyroshelf.free")
    else
        system.openURL("market://details?id=com.pyroshelf.free")
    end
end

local function onSystemEvent(e)
    if e.type == "applicationStart" then
    elseif e.type == "applicationExit" then
        save()
    end
end

function removeSystemListener()
    Runtime:removeEventListener("system", onSystemEvent)
end

Runtime:addEventListener("system", onSystemEvent)