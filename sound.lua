module(..., package.seeall)

require "audio"
require "settings"
--require "media"

--local themeMusic = audio.loadStream("theme.mp3")
--local themeChannel
--local clickSound = media.newEventSound("clicky.mp3")

local soundNames = {
    "blast",
    "touch",
    "click",
    "rocket",
    "applause",
    "aww",
    "bulb1",
    "bulb2",
    "swish",
    "locked",
    "unlock"
}
local sounds = {}

for i = 1, #soundNames do
    local name = soundNames[i]
    sounds[name] = audio.loadSound(name .. ".wav")
end

function play(name, ...)
    if settings.data.soundsOn then
        audio.play(sounds[name], ...)
    end
end

--function isThemePlaying()
--    return not not themeChannel
--end
--
--function playTheme()
--    if not themeChannel then
--        themeChannel = audio.play(themeMusic, {fadein=3000, onComplete=function()
--            print("theme done")
--            themeChannel = nil
--        end})
--    end
--end
--
--function stopTheme(instant)
--    if not themeChannel then return end
--
--    if instant then
--        audio.stop(themeChannel)
--    else
--        -- buggy: after fadeOut is done the channel volume isn't restored
--        audio.fadeOut({ channel=themeChannel, time=1200 })
--    end
--end