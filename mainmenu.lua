module(..., package.seeall)

require "sprite"
require "screen"
require "settings"
require "gameNetwork"

--require "inneractive"
--require "admob"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
--local CSX = display.contentScaleX
--local CSY = display.contentScaleY

local CSX = screen.getCSX()
local CSY = CSX

local ssBomb

local function loadSprites()
    ssBomb = screen.loadSpriteSheet("pyro_ss")
    ssBomb:add("1", 1, 10, 800, 0)
    ssBomb:add("2", 11, 10, 800, 0)
    ssBomb:add("3", 21, 10, 800, 0)
    ssBomb:add("4", 31, 12, 1000, 0)
end

local function unloadSprites()
    ssBomb:dispose()
end

local function newBomb(params)
    local bomb = ssBomb:newSprite()

    bomb.x = params.x
    bomb.y = params.y

    function bomb:setState(state)
        self:prepare(tostring(state))
        self:play()
    end

    bomb:setState(params.state)

    return bomb
end

local function createSplash()
    local group = display.newGroup()

    local blackRect = display.newRect(SOX, SOY, CW - SOX * 2, CH - SOY * 2)
    blackRect:setFillColor(0, 0, 0, 255)
    group:insert(blackRect)

    -- creating white background
    local whiteRect = display.newRect(SOX, SOY, CW - SOX * 2, CH - SOY * 2)
    whiteRect:setFillColor(255, 255, 255, 255)
--    whiteRect:setFillColor(238, 243, 250, 255)
    whiteRect.alpha = 0
    group:insert(whiteRect)

    local logo = screen.newScaledImage("logo")
    logo.alpha = 0
    logo.x = CW/2
--    logo.y = CH/2 - 400
    logo.y = SOY - logo.contentHeight - 10
    group:insert(logo)

    -- delay to let the engine finish initializing whatever stuff before showing logo fade-in animation,
    -- can also use it to load some of our own resources, e.g. sounds
    local preLogoDelay = 500
    -- delay to let players take a good look at the logo before it fades out
    -- and load/init some additional stuff
    local logoDelay = 1000
    local logoSlideOutTime = 250

    local function killGroup()
--        inneractive.showAd(1, SOX, SOY)
--        admob.showAd(true)
        group:removeSelf()
    end

    local function logoSlideOut()
        -- initialize the app on OpenFeint during "logoDelay"
        gameNetwork.init( "openfeint",
            settings.feint.productKey, settings.feint.productSecret,
            settings.feint.displayName, settings.feint.appId )

        transition.to(logo, {time=logoSlideOutTime, delay=logoDelay, alpha=0, y=CH/2 + 400})

        -- rect fade-out
        blackRect:removeSelf()
        transition.to(whiteRect, {time=750, delay=logoDelay+logoSlideOutTime, alpha=0,
            transition=easing.outQuad, onComplete=killGroup})
    end

    local function logoSlideIn()
        -- rect fade-in
        transition.to(whiteRect, {time=500, delay=preLogoDelay, alpha=1})

        transition.to(logo, {time=1500, delay=preLogoDelay, alpha=1, y=CH/2,
            transition=easing.outExpo, onComplete=logoSlideOut})
    end

    -- block the underneath UI during logo's animation
    group:addEventListener("touch", function() return true end)

    -- take a moment to load sounds during "preLogoDelay"
    require "sound"

    logoSlideIn()

    return group
end

local function createTitle()
    local lg = display.newGroup()

    local shelfGlass = screen.newScaledImage("shelf_glass")
    shelfGlass.x = CW / 2
    shelfGlass.y = CH / 2 - 120

    local shelfText = screen.newScaledImage("shelf_text")
    shelfText.x = CW / 2
    shelfText.y = CH / 2 - 90

    local center = CW / 2
    local top = 100
    local state = math.random(1, 2)

--    settings.data.jumpingPlayButton = not settings.data.jumpingPlayButton
--    state = settings.data.jumpingPlayButton and 1 or 2
--    print("STATE", state)

    local pBomb = newBomb{x = center - 75, y = top, state = 1}
    local lBomb = newBomb{x = center - 27, y = top, state = 2}
    local aBomb = newBomb{x = center + 27, y = top, state = 3}
    local yBomb = newBomb{x = center + 75, y = top, state = 4}

    lg:insert(shelfGlass)
    lg:insert(shelfText)
    lg:insert(pBomb)
    lg:insert(lBomb)
    lg:insert(aBomb)
    lg:insert(yBomb)

    lg:setReferencePoint(display.CenterReferencePoint)

    return lg
end

-- Main function - MUST return a display.newGroup()
function new(splash)
    local menuGroup = display.newGroup()

    loadSprites()

    local bg = screen.newScaledImage("main_bg")
    bg:setReferencePoint(display.TopCenterReferencePoint)
    bg.x = CW / 2
    bg.y = SOY

    local playBtn = screen.newButton{
        default = "button",
        over = "button_over",
        text = "PLAY",
        textOffsetY = settings.isDevice and -1 or 1,
        fontName = settings.mainFont,
        onTap = function()
            sound.play("click")
            director:changeScene("worldmenu")
        end
    }
    playBtn.x = CW / 2
    playBtn.y = CH / 2 + 40

--    local extrasBtn = screen.newButton{
--        default = "button",
--        over = "button_over",
--        text = "Get Picklocks",
--        textOffsetY = settings.isDevice and -1 or 0,
--        fontName = settings.mainFont,
--        onTap = function()
--            sound.play("click")
----            native.cancelWebPopup()
----            twitter.tweetit()
----            director:changeScene("about")
--        end
--    }
--    extrasBtn.x = CW / 2
--    extrasBtn.y = CH / 2 + 100

    -- local keyBtn = screen.newButton({
    --     default = "key",
    --     overScale = 1.2,
    --     onTap = function()
    --         sound.play("click")
    --         director:changeScene("earn")
    --     end
    -- })
    -- keyBtn:setReferencePoint(display.CenterReferencePoint)
    -- keyBtn.x = CW / 2 - 30
    -- keyBtn.y = CH / 2 + 100

--    local aboutBtn = screen.newButton({
--        default = "about",
--        overScale = 1.2,
--        onTap = function()
--            sound.play("click")
--            director:changeScene("about")
--        end
--    })
--    aboutBtn:setReferencePoint(display.CenterReferencePoint)
--    aboutBtn.alpha = 0.5
--    aboutBtn.x = CW / 2
--    aboutBtn.y = CH / 2 + 100

    local aboutBtn = screen.newButton{
        default = "unlock",
        text = "ABOUT",
        fontSize = 20,
        fontName = settings.mainFont,
        textOffsetY = settings.isDevice and 0 or 1,
        onTap = function(e)
            sound.play("click")
            director:changeScene("about")
        end
    }
    aboutBtn:setReferencePoint(display.CenterReferencePoint)
    aboutBtn.x = CW / 2
    aboutBtn.y = CH / 2 + 100

--    local aboutBtn = screen.newButton{
--        default = "button",
--        over = "button_over",
--        text = "ABOUT",
--        textOffsetY = settings.isDevice and -1 or 0,
--        fontName = settings.mainFont,
--        onTap = function()
--            sound.play("click")
--            director:changeScene("about")
--        end
--    }
--    aboutBtn.x = CW / 2
--    aboutBtn.y = CH / 2 + 160

    local openFeintBtn = screen.newButton{
        default = "openfeint",
        overScale = 1.2,
        onTap = function()
            gameNetwork.show( "leaderboards" )
        end
    }
    openFeintBtn.x = 34
    openFeintBtn.y = CH - SOY - 32

   local facebook = screen.newTouchImage("facebook", function()
       system.openURL("http://www.facebook.com/PyroShelf")
   end)
   facebook:setReferencePoint(display.TopLeftReferencePoint)
   facebook.x = CW - SOX - facebook.contentWidth - 10
   facebook.y = CH - SOY - facebook.contentHeight - 10

    menuGroup:insert(bg)
    local title = createTitle()
    title.y = title.y + 50
    menuGroup:insert(title)
    menuGroup:insert(playBtn)
    -- menuGroup:insert(keyBtn)
    menuGroup:insert(aboutBtn)
    menuGroup:insert(openFeintBtn)
    menuGroup:insert(facebook)

--    local adFree = screen.newScaledImage("ad_free")
--    adFree:setReferencePoint(display.TopCenterReferencePoint)
--    adFree.x = CW / 2
--    adFree.y = SOY
--    menuGroup:insert(adFree)

--    local craftBtn = screen.newButton{
--        default = "button",
--        over = "button_over",
--        text = "CRAFT",
--        textOffsetY = settings.isDevice and -1 or 0,
--        fontName = settings.mainFont,
--        onTap = function()
--            director:changeScene("editor")
--        end
--    }
--    craftBtn.x = CW / 2
--    craftBtn.y = CH / 2 + 160
--    menuGroup:insert(craftBtn)

    if splash then
        menuGroup:insert(createSplash())
    else
        -- this require is for emulator only
        require "sound"
--        inneractive.showAd(1, SOX, SOY)
--        admob.showAd(true)
    end

--    Data.soundsOn = false
--    settings.Data.currentWorld = 9
--    _G.Data.currentWorld = 9
--    print(Data.currentWorld)

    return menuGroup
end

function clean()
    unloadSprites()
end

--local function setAdVisible(visible)
--    if settings.isFreeVersion then
--        local sceneName = director:getCurrentSceneName()
--        if "mainmenu" == sceneName or "worldmenu" == sceneName or "levelmenu" == sceneName then
--            if visible then
--                inneractive.showAd(1, SOX, SOY)
--            else
--                inneractive.hideAd()
--            end
--        end
--    end
--end
--
--local function onSystemEvent(e)
--    if e.type == "applicationSuspend" then
--        setAdVisible(false)
--    elseif e.type == "applicationResume" then
--        setAdVisible(true)
--    end
--end
--
--Runtime:addEventListener("system", onSystemEvent)
