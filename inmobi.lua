module(..., package.seeall)

require "ads"

local isDevice = system.getInfo("environment") == "device"

local adNetwork = "inmobi"
local appID = "4028cba6328f45a10132bfcc7bb9030a" -- activated
--local appID = "4028cb962895efc50128fc993f9f025a" -- test
local adsTable = {
    "banner320x48",
    "banner300x250",
    "banner728x90",
    "banner468x60",
    "banner120x600"
}
local resTable = {
    {x=320, y=48},
    {x=300, y=250},
    {x=728, y=90},
    {x=468, y=60},
    {x=120, y=600},
}
local dummyAd
local adPosX, adPosY
local dummyTimer
local isAdVisible = false

function init()
    if isDevice then
        ads.init( adNetwork, appID )
    end
end

local function stayOnTop()
    dummyAd:toFront()
end

function showAd(adIndex, x, y)
    if isAdVisible then
        if (adPosX == x and adPosY == y) then
            return
        else
            hideAd()
        end
    end

    adPosX = x
    adPosY = y

    if isDevice then
        ads.show(adsTable[adIndex], { x=x, y=y, interval=5, testMode=true })
    else
        dummyAd = display.newRect(x, y, resTable[adIndex].x, resTable[adIndex].y)
        dummyTimer = timer.performWithDelay(1000, stayOnTop, 0)
    end
    isAdVisible = true
end

function hideAd()
    if isDevice then
        ads.hide()
    else
        if dummyTimer then
            timer.cancel(dummyTimer)
            dummyTimer = nil
        end
        if dummyAd then
            dummyAd:removeSelf()
            dummyAd = nil
        end
    end
    isAdVisible = false
end

