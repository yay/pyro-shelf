module(..., package.seeall)

require "screen"
require "sound"
--require "inneractive"
--require "admob"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
--local CSX = display.contentScaleX
--local CSY = display.contentScaleY

local CSX = screen.getCSX()
local CSY = CSX

local isWVGA = screen.width == 480

function new()
--    inneractive.hideAd()
--    admob.hideAd()

    local lg = display.newGroup()

    local bg = screen.newScaledImage("about_bg")
    bg:setReferencePoint(display.TopCenterReferencePoint)
    bg.x = CW / 2
    bg.y = SOY

    local pyro = screen.newScaledImage("pyro")
    local shelfGlass = screen.newScaledImage("shelf_glass")
    local shelfText = screen.newScaledImage("shelf_text")

    pyro.x = CW / 2
    shelfGlass.x = CW / 2
    shelfText.x = CW / 2

    pyro.y = SOY + 54
    shelfGlass.y = SOY + 78
    shelfText.y = SOY + 110

    local version = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    version:setTextColor(0, 0, 0)
    screen.updateText(version, "Version 1.2", CW/2, SOY+134, display.TopCenterReferencePoint)

    local website = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    website:setTextColor(0, 0, 0)
    website.alpha = 0.6
    screen.updateText(website, "www.pyroshelf.com", CW/2, SOY+170, display.TopCenterReferencePoint)
--    function website:touch(e)
--        if e.phase == "began" then
--            system.openURL("http://pyroshelf.com/")
--        end
--    end
--    website:addEventListener("touch", website)

    local author = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    author:setTextColor(0, 0, 0)
    author.alpha = 0.6
    screen.updateText(author, "Author     Developer", CW/2, SOY+220, display.TopCenterReferencePoint)

    local amp = screen.newScaledImage("amp")
    amp.alpha = 0.6
    amp.x = CW / 2 - 16.5
    amp.y = SOY + 236

    local vitaly = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    vitaly:setTextColor(0, 0, 0)
    screen.updateText(vitaly, "Vitaly Kravchenko", CW/2, SOY+250, display.TopCenterReferencePoint)

    local rate = screen.newMultiLineText({
            "Enjoy playing Pyro Shelf?",
            "Help support the app",
            "by rating it 5 stars",
            "on the Marketplace."
        },
        CW / 2, CH - 190,
        settings.mainFont, 18, display.TopCenterReferencePoint)
    rate.alpha = 0.6

    local rateBtn = screen.newButton{
        default = "button",
        over = "button_over",
        text = "Visit Marketplace",
        textOffsetY = settings.isDevice and -1 or 1,
        fontName = settings.mainFont,
        fontSize = 22,
        onTap = function()
            sound.play("click")
            settings.openMarketPage()
        end
    }
    rateBtn.x = CW / 2
    rateBtn.y = rate.y + rate.contentHeight + 30 --CH - 80

    local backBtn = screen.newButton({
        default = "back",
        overScale = 1.2,
        onTap = function()
            sound.play("click")
            director:changeScene("mainmenu")
        end
    })
    backBtn:setReferencePoint(display.BottomLeftReferencePoint)
    backBtn.x = 10
    backBtn.y = CH - SOY - 10

    lg:insert(bg)
    lg:insert(shelfGlass)
    lg:insert(pyro)
    lg:insert(shelfText)
    lg:insert(version)
    lg:insert(website)
    lg:insert(author)
    lg:insert(amp)
    lg:insert(vitaly)
    lg:insert(rate)
    lg:insert(rateBtn)
    lg:insert(backBtn)

    return lg
end