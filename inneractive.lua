module(..., package.seeall)

require "ads"

local isDevice = system.getInfo("environment") == "device"
local isAdVisible = false
local isInitDone = false

local adNetwork = "inneractive"
local appID = "VITSOFTWARE_PyroShelf_Android"

local adsTable = {
    "banner",
    "fullscreen",
    "text"
}

local adPosX, adPosY
local winAdImpressions = 0

local function adListener( event )
    if event.isError then
        -- Failed to receive an ad.
--        native.showAlert("inneractive", "Failed to receive an ad.")
        hideAd()
    end
end

if isDevice and not isInitDone then
    ads.init( adNetwork, appID, adListener )
    isInitDone = true
end

function showAd(adIndex, x, y, winAd)
    if isAdVisible then
        if adIndex ~= 2 and (adPosX == x and adPosY == y) then -- same position, nothing to do
            return
        else
            hideAd() -- hide existing ad before showing a new one
        end
    end

    adPosX = x
    adPosY = y

    if isDevice then
        if winAd then
            winAdImpressions = winAdImpressions + 1
            if winAdImpressions % 3 == 0 then
                ads.show( adsTable[2], { x=0, y=0, interval=60 } )  -- fullscreen ( every third ad)
            else
                ads.show( adsTable[adIndex], { x=x, y=y, interval=30 } )
            end
        else
            ads.show( adsTable[adIndex], { x=x, y=y, interval=30 } )
        end
    end
    isAdVisible = true
end

function hideAd()
    if isDevice then
        ads.hide()
    end
    isAdVisible = false
end

