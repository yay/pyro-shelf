module(..., package.seeall)

require "physics"
require "sprite"

require "screen"
require "filters"

local function round(n)
    return math.floor(n + 0.5)
end

local bullet_explode = "bullet_explode" .. screen.imageSuffix
require (bullet_explode)

local velocity = 180
--local velocity = 45

local bulletSpriteSheetData = package.loaded[bullet_explode].getSpriteSheetData()
local bulletSpriteSheet = sprite.newSpriteSheetFromData(
    "images/" .. bullet_explode .. ".png", bulletSpriteSheetData)
local bulletSpriteSet = sprite.newSpriteSet(bulletSpriteSheet, 1, 3)

local ssBullet = screen.loadSpriteSheet("fireball_ss")
ssBullet:add("burn", 1, 6, 300, 0)

onEvent = nil

local function callEvent(params)
    if type(onEvent) == "function" then
        onEvent(params)
    end
end

function new(params)

--    local bullet = display.newImageRect("images/bullet.png", 8, 8)
    local bullet = ssBullet:newSprite()
    bullet.name = "bullet"
    bullet.radius = bullet.width / 6 - 1

    bullet.x = params.bomb.x
    bullet.y = params.bomb.y

    physics.addBody(bullet, "dynamic", {radius=bullet.radius/2, friction=0, bounce=1,
        filter=filters.bullet})

    function bullet:flyTo(direction)

        local angle = direction * math.pi / 180
        local offset = 0 -- self.radius

        self.rotation = direction
        self.x = self.x + offset * math.sin(angle)
        self.y = self.y - offset * math.cos(angle)

        local vx = round( velocity * math.sin(angle) )
        local vy = round(-velocity * math.cos(angle) )

--        self.linearDamping = 1
        self:setLinearVelocity(vx, vy)

        -- enable collisions after the bullet has left the bomb
--        local sensorDelay = (self.radius + params.bomb.radius + safeDistance) / velocity * 1000
--        timer.performWithDelay(sensorDelay, function()
--            self.isSensor = false
--        end)
    end

    function bullet:destroy()
        timer.performWithDelay(0, function()
            callEvent({name = "destroy"})
        end)
        self:removeSelf()
    end

    function bullet:blast()
        local bulletSprite = sprite.newSprite(bulletSpriteSet)
        bulletSprite.xScale = display.contentScaleX
        bulletSprite.yScale = display.contentScaleY
        self.parent:insert(bulletSprite)
        bulletSprite.x = self.x
        bulletSprite.y = self.y
        function bulletSprite:sprite(e)
            if e.phase == "loop" then
                self:removeEventListener("sprite", self)
                self:removeSelf()
            end
        end
        bulletSprite:addEventListener("sprite", bulletSprite)
        bulletSprite:play()

        self:destroy()
--        timer.performWithDelay(0, function()
--            callEvent({name = "destroy"})
--        end)
--        self:removeSelf()
    end

--    function bullet:preCollision(e)
--        if e.other.name == "bomb" then
--            if e.other.id == params.bomb.id then
--                -- self hit
----                self.isSensor = true
--            else
--            end
--        end
--    end

    function bullet:collision(e)
        -- using the 'dead' flag,
        -- as otherwise we can receive a row of collision events very quickly
        -- and have multiple calls to blast()
        if e.other.name == "wall" and not self.dead then
            self.dead = true
            timer.performWithDelay(0, function()
                self:blast()
            end)
        end
    end

--    function bullet:postCollision(e)
--        if e.other.name == "bomb" then
--            if e.other.id == params.bomb.id then
--                -- self hit
--                self.isSensor = true
--            else
--            end
--        end
--    end

--    bullet:addEventListener("preCollision", bullet)
    bullet:addEventListener("collision", bullet)
--    bullet:addEventListener("postCollision", bullet)

    callEvent({name = "create"})

    bullet:prepare("burn")
    bullet:play()

    return bullet
end