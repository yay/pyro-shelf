module(..., package.seeall)

require "sprite"
require "physics"
--require "audio"
require "sound"

require "screen"
require "filters"
require "bullet"

--local hitSound = audio.loadSound("hit.wav")
--local fireballSound = audio.loadSound("fireball.wav")
--local blast1 = audio.loadSound("blast1.wav")
--local blast2 = audio.loadSound("blast2.wav")
--local blast3 = audio.loadSound("blast3.wav")
--local blastSound = audio.loadSound("blast.wav")
local playSound = sound.play

local lastBlastTime = 0
local lastHitTime = 0
local activeRays = 0

local ssBlast = screen.loadSpriteSheet("blast_ss")
ssBlast:add("blast", 1, 9, 500, 1)

local ssBomb = screen.loadSpriteSheet("bomb_ss")
ssBomb:add("1_1", 1, 10, 800, 0)
ssBomb:add("1_2", 4, 7, 560, 1)
ssBomb:add("1_3", 7, 4, 320, 1)

ssBomb:add("2_1", 11, 10, 800, 0)
ssBomb:add("2_2", 14, 7, 560, 1)
ssBomb:add("2_3", 17, 4, 320, 1)

ssBomb:add("3_1", 21, 10, 800, 0)
ssBomb:add("3_2", 24, 7, 560, 1)
ssBomb:add("3_3", 27, 4, 320, 1)

ssBomb:add("4_1", 31, 12, 1000, 0)
ssBomb:add("4_2", 37, 6, 500, 1)
ssBomb:add("4_3", 39, 4, 333, 1)

local ssSpark = screen.loadSpriteSheet("spark_ss", "")
for i = 1, ssSpark.frameCount do
    ssSpark:add(tostring(i), i, 1, 100, 1)
end

onEvent = nil

local function callEvent(params)
    if type(onEvent) == "function" then
        onEvent(params)
    end
end

function createRays(parent, x, y)
    local ray, angle, distance

    for i = 1, 20 do
        ray = ssSpark:newSprite(false)
        ray:prepare( tostring(math.random(1, ssSpark.frameCount)) )
        ray:scale(0.25, 0.25)
        ray:setReferencePoint(display.BottomCenterReferencePoint)
        ray.x = x
        ray.y = y
        ray.rotation = math.random(110, 250)

        function ray:clean()
            -- need to cancel ray's "decay" transition, to prevent call to onComplete
            -- when the object will no longer exist
            if self.decay then
                transition.cancel(self.decay)
            end
        end

        function ray:kill()
            self:removeSelf()
        end

        distance = math.random(50, 200)
        angle = ray.rotation * math.pi / 180
        ray.x = x + 5 * math.sin(angle)
        ray.y = y - 5 * math.cos(angle)
        ray.nx = ray.x + distance * math.sin(angle)
        ray.ny = ray.y - distance * math.cos(angle)

        parent:insert(ray)

        ray.decay = transition.to(ray, {time=2000, x = ray.nx, y = ray.ny, alpha=0,
                transition=easing.outExpo,
            onComplete=ray.kill})
    end
end

function new(params)
    -- assuming turrets can only be created on level loading
    activeRays = 0
--    local bomb = display.newImageRect(
--        "images/turret_" .. tostring(params.state) .. ".png", 64, 64)
    local bomb = display.newGroup()
    bomb.name = "bomb"
    bomb.id = {}
    bomb.radius = 5
    bomb.x = params.x
    bomb.y = params.y
    bomb.canTouch = params.canTouch

    local bombSprite = ssBomb:newSprite()
    bomb:insert(bombSprite)

    function bombSprite:sprite(e)
        if e.phase == "end" then
            local s = self.sequence
            if s == "1_2" or s == "1_3" then
                self:prepare("1_1")
                self:play()
            elseif s == "2_2" or s == "2_3" then
                self:prepare("2_1")
                self:play()
            elseif s == "3_2" or s == "3_3" then
                self:prepare("3_1")
                self:play()
            elseif s == "4_2" or s == "4_3" then
                self:prepare("4_1")
                self:play()
            end
        end
    end

    function bomb:setState(state, randomize)
        self.state = state

        if randomize then
            bombSprite:prepare( tostring(state) .. "_" .. tostring(math.random(1, 3)) )
        else
            bombSprite:prepare( tostring(state) .. "_1" )
        end
        bombSprite:play()
    end

    function bomb:createFlash()
        local flash = ssBlast:newSprite()
        -- rotation is set to 0, 90 or 270 degrees randomly
        -- (omit the 180 value, as sprite doesn't look natural upside down)
        flash.rotation = (math.pow(2, math.random(0, 2)) - 1) * 90
        self.parent:insert(flash)
        flash.x = self.x
        flash.y = self.y
        function flash:sprite(e)
            if e.phase == "end" then
                self:removeEventListener("sprite", self)
                self:removeSelf()
            end
        end
        flash:addEventListener("sprite", flash)
        flash:prepare("blast")
        flash:play()
    end

    function bomb:createRays()
        local ray, rays, angle, distance

        -- degradation rule for better performance
        if activeRays >= 25 then
            rays = 1
        elseif activeRays >= 23 then
            rays = 2
        elseif activeRays >= 20 then
            rays = 3
        elseif activeRays >= 16 then
            rays = 4
        elseif activeRays >= 10 then
            rays = 6
        else
            rays = 10
        end

        for i = 1, rays do
            ray = ssSpark:newSprite(false)
            ray:scale(0.6, 0.6)
            activeRays = activeRays + 1
--            print("Active rays:", activeRays)
            ray:prepare( tostring(math.random(1, ssSpark.frameCount)) )
            ray:setReferencePoint(display.BottomCenterReferencePoint)
            ray.x = self.x
            ray.y = self.y
            ray.rotation = math.random(0, 359)

            function ray:clean()
                -- need to cancel ray's "decay" transition, to prevent call to onComplete
                -- when the object will no longer exist
                if self.decay then
                    transition.cancel(self.decay)
                end
            end
            function ray:kill()
                activeRays = activeRays - 1
--                print("Active rays:", activeRays)
                self:removeSelf()
            end

            distance = math.random(50, 100)
            angle = ray.rotation * math.pi / 180
            ray.nx = ray.x + distance * math.sin(angle)
            ray.ny = ray.y - distance * math.cos(angle)

            self.parent:insert(ray)

--            ray:play()
            ray.decay = transition.to(ray, {time=750, x = ray.nx, y = ray.ny, alpha=0,
--                transition=easing.outQuad,
                onComplete=ray.kill})
        end
    end

    function bomb:clean()
        self:removeEventListener("collision", self)
        bombSprite:removeEventListener("sprite", bombSprite)

        if self.hitTween then
            transition.cancel(self.hitTween)
        end
    end

    function bomb:createBullets()
        local north = bullet.new({bomb = self})
        local east  = bullet.new({bomb = self})
        local south = bullet.new({bomb = self})
        local west  = bullet.new({bomb = self})

        self.parent:insert(north)
        self.parent:insert(east)
        self.parent:insert(south)
        self.parent:insert(west)

        north:flyTo(0)
        east:flyTo(90)
        south:flyTo(180)
        west:flyTo(270)
    end

    function bomb:blast()
        if not self.dead then
            self.dead = true
            -- Setting isSensor to true to prevent self-spawned bullets from colliding with self.
            -- Sensors don't fire pre- or post-collisions events. They only fire on the collision event.
            -- isSensor property is the only body property that _always_ returns nil.
            self.isSensor = true

            self:clean()

            -- performance optimisation
            local time = system.getTimer()
            if time - lastBlastTime > 200 then
                playSound("blast")
--                local n = math.random(1, 3)
--                if n == 1 then
--                    audio.play(blast1)
--                elseif n == 2 then
--                    audio.play(blast2)
--                else
--                    audio.play(blast3)
--                end
            end
            lastBlastTime = time
--            print("Free channels:", audio.freeChannels)

            self:createFlash()
            self:createRays()
            self:createBullets()

            timer.performWithDelay(0, function()
                callEvent({name = "destroy"})
            end)
            self:removeSelf()
        end
    end

    function bomb:trigger()
        if self.state < 4 then
            self:setState(self.state + 1)
        else
            self:blast()
        end
    end

    function bomb:touch(e)
        if e.phase == "began" then
            if self.canTouch() then
                if self.state < 4 then
                    if not self.hitTween then
                        local this = self
                        local sx = self.xScale
                        local sy = self.yScale
                        local k = 1.5

                        callEvent({name = "touch"})
                        playSound("touch")
                        bombSprite:pause()
                        this.hitTween = transition.to(this, {time=100, xScale=k*sx, yScale=k*sy, onComplete=function()
                            this.hitTween = transition.to(this, {time=200, xScale=sx, yScale=sy, onComplete=function()
                                this:trigger()
                                this.hitTween = nil
                            end})
                        end})
                    end
                else
                    self:trigger()
                    callEvent({name = "touch"})
                end
            end
        end
        return true
    end

    function bomb:collision(e)
        if e.other.name == "bullet" then
            local bombDead = self.state == 4
            timer.performWithDelay(0, function()
--                local time = system.getTimer()
--                if self.state < 4 and time - lastHitTime > 200 then
--                    sound.play("hit")
--                    lastHitTime = time
--                end
                -- trigger the bomb
                bomb:trigger()
                -- destroy the bullet
                if bombDead then
                    -- destroy the bullet right away, without showing the burst,
                    -- as it won't be visible anyway when bomb explodes
                    e.other:destroy()
                else
                    e.other:blast()
                end
            end)
        end
    end

    physics.addBody(bomb, "static", {radius=bomb.radius, friction=0, bounce=1,
        filter=filters.bomb})

    bombSprite:addEventListener("sprite", bombSprite)

    bomb:addEventListener("touch", bomb)

    bomb:addEventListener("collision", bomb)

    bomb:setState(params.state or 1, true)

    callEvent({name = "create"})

    return bomb
end