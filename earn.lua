module(..., package.seeall)

require "screen"
require "sound"
--require "inneractive"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
--local CSX = display.contentScaleX
--local CSY = display.contentScaleY

local CSX = screen.getCSX()
local CSY = CSX

function new()
--    inneractive.hideAd()

    local lg = display.newGroup()

    local bg = screen.newScaledImage("earn_bg")
--    bg:scale(-1, -1)
--    bg:setReferencePoint(display.BottomCenterReferencePoint)
    bg:setReferencePoint(display.TopCenterReferencePoint)
    bg.x = CW / 2
    bg.y = SOY

    local earnRect = display.newRect(SOX, 20, CW - SOX * 2, 76)
    earnRect:setFillColor(0, 0, 0, 128)

    local earnText = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    local unlockText = screen.newScaledText("", 0, 0, settings.mainFont, 24)
    screen.updateText(earnText, "Earn Picklocks", CW/2, 30, display.TopCenterReferencePoint)
    screen.updateText(unlockText, "to Unlock New Worlds!", CW/2, 60, display.TopCenterReferencePoint)

    local backBtn = screen.newButton({
        default = "back",
        overScale = 1.2,
        onTap = function()
            sound.play("click")
            director:changeScene("mainmenu")
        end
    })
    backBtn:setReferencePoint(display.BottomLeftReferencePoint)
    backBtn.x = 10
    backBtn.y = CH - SOY - 10

    local rateBtn = screen.newScaledImage("rate")

    local likeBtn = screen.newScaledImage("like")

    local tweetBtn = screen.newScaledImage("empty")
    tweetBtn.alpha = 0.8

    local yDelta = (backBtn.y - backBtn.contentHeight - earnRect.y - earnRect.contentHeight) / 3
    local yStart = earnRect.y + earnRect.contentHeight + 34
    local xOffset = 30

    rateBtn.x = CW / 2 + xOffset
    likeBtn.x = CW / 2 + xOffset
    tweetBtn.x = CW / 2 + xOffset

    rateBtn.y = yStart + yDelta * 0
    likeBtn.y = yStart + yDelta * 1
    tweetBtn.y = yStart + yDelta * 2

    local rateKey = screen.newScaledImage("50_key")
    rateKey.x = SOX + 42
    rateKey.y = rateBtn.y + 4

    local likeKey = screen.newScaledImage("50_key")
    likeKey.x = SOX + 42
    likeKey.y = likeBtn.y + 4

    local tweetKey = screen.newScaledImage("50_key")
    tweetKey.alpha = 0.6
    tweetKey.x = SOX + 42
    tweetKey.y = tweetBtn.y + 4

--    local rateBtn = screen.newButton{
--        default = "unlock",
--        text = "RATE",
--        fontSize = 20,
--        fontName = settings.mainFont,
--        textOffsetY = settings.isDevice and 0 or 1,
--        onTap = function(e)
--        end
--    }
--    rateBtn.x = CW / 2
--    rateBtn.y = CH / 2 - 60
--
--    local tweetBtn = screen.newButton{
--        default = "unlock",
--        text = "TWEET",
--        fontSize = 20,
--        fontName = settings.mainFont,
--        textOffsetY = settings.isDevice and 0 or 1,
--        onTap = function(e)
--        end
--    }
--    tweetBtn.x = CW / 2
--    tweetBtn.y = CH / 2
--
--    local likeBtn = screen.newButton{
--        default = "unlock",
--        text = "LIKE",
--        fontSize = 20,
--        fontName = settings.mainFont,
--        textOffsetY = settings.isDevice and 0 or 1,
--        onTap = function(e)
--        end
--    }
--    likeBtn.x = CW / 2
--    likeBtn.y = CH / 2 + 60

    lg:insert(bg)
    lg:insert(earnRect)
    lg:insert(earnText)
    lg:insert(unlockText)

    lg:insert(rateBtn)
    lg:insert(likeBtn)
    lg:insert(tweetBtn)

    lg:insert(rateKey)
    lg:insert(likeKey)
    lg:insert(tweetKey)

    lg:insert(backBtn)

    return lg
end