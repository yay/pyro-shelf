display.setStatusBar( display.HiddenStatusBar )

require "settings"
require "director"
require "screen"
--require "inneractive"

local function main()
    -- show main menu and splash screen, if running on device
    director:changeScene("mainmenu", settings.isDevice)
--    director:changeScene("mainmenu", true)

    return true
end

local function onQuitAlert(e)
    if "clicked" == e.action then
        if e.index == 1 then
            settings.save()
            -- this will prevent us from saving settings again on "applicationExit" event
            settings.removeSystemListener()
            if system.getInfo( "platformName" ) == "Android" then
                native.requestExit()
            else
                os.exit()
            end
        end
--    elseif "cancelled" == e.action then
    end
end

-- since we use "lazy" loading of the "sound" module in "mainmenu",
-- it won't be available here right away, so we do a check
local function playClick()
    if package.loaded["sound"] then
        package.loaded["sound"].play("click")
    end
end

local function onKeyEvent(e)
    local returnValue = false

    if e.phase == "down" then

        local sceneName = director:getCurrentSceneName()

        if "back" == e.keyName then

            if "mainmenu" == sceneName then
                returnValue = true
                playClick()
                local quitAlert = native.showAlert("Pyro Shelf", "Are you sure you want to quit?",
                    {"Yes", "No"}, onQuitAlert)
            elseif "worldmenu" == sceneName or "about" == sceneName or "earn" == sceneName then
                returnValue = true
                playClick()
                director:changeScene("mainmenu")
            elseif "levelmenu" == sceneName then
                returnValue = true
                playClick()
                director:changeScene("worldmenu", settings.data.currentWorld)
--            elseif "level" == sceneName then
--                director:changeScene("levelmenu", settings.data.currentWorld)
            elseif "level" == sceneName then
--                inneractive.hideAd()
            end

        elseif "center" == e.keyName then
            -- take screenshot
            -- note: requires "android.permission.WRITE_EXTERNAL_STORAGE"
--            local object = display.captureScreen( true )
--            object:removeSelf()
        end
    end

    return returnValue
end

Runtime:addEventListener("key", onKeyEvent)

settings.load()

-- print all globar variables defined in current environment
--for n in pairs(_G) do print(n) end

main()