application =
{
    content =
    {
        fps = 30,

        width = 320,
        height = 480,

        scale = "letterbox",

--        antialias = true,

        imageSuffix =
        {
            ["_480"] = 1.5,
            ["_640"] = 2
        }
    }
}