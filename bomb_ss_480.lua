module(...)

-- This file is for use with Corona Game Edition
-- 
-- The function getSpriteSheetData() returns a table suitable for importing using sprite.newSpriteSheetFromData()
-- 
-- This file is automatically generated with TexturePacker (http://texturepacker.com). Do not edit
-- 
-- 
-- Usage example:
--			local sheetData = require "ThisFile.lua"
--          local data = sheetData.getSpriteSheetData()
--			local spriteSheet = sprite.newSpriteSheetFromData( "Untitled.png", data )
-- 
-- For more details, see http://developer.anscamobile.com/content/game-edition-sprite-sheets

function getSpriteSheetData()
    local sheet = {
        frames = {
            {
                name = "01_01.png",
                spriteColorRect = { x = 17, y = 11, width = 76, height = 72 },
                textureRect = { x = 0, y = 0, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_02.png",
                spriteColorRect = { x = 19, y = 13, width = 74, height = 72 },
                textureRect = { x = 76, y = 0, width = 74, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_03.png",
                spriteColorRect = { x = 21, y = 13, width = 72, height = 76 },
                textureRect = { x = 150, y = 0, width = 72, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_04.png",
                spriteColorRect = { x = 21, y = 18, width = 72, height = 76 },
                textureRect = { x = 0, y = 76, width = 72, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_05.png",
                spriteColorRect = { x = 24, y = 21, width = 70, height = 74 },
                textureRect = { x = 72, y = 76, width = 70, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_06.png",
                spriteColorRect = { x = 23, y = 23, width = 72, height = 72 },
                textureRect = { x = 142, y = 76, width = 72, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_07.png",
                spriteColorRect = { x = 24, y = 19, width = 70, height = 76 },
                textureRect = { x = 0, y = 152, width = 70, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_08.png",
                spriteColorRect = { x = 22, y = 15, width = 72, height = 76 },
                textureRect = { x = 70, y = 152, width = 72, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_09.png",
                spriteColorRect = { x = 19, y = 13, width = 74, height = 74 },
                textureRect = { x = 142, y = 152, width = 74, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "01_10.png",
                spriteColorRect = { x = 19, y = 10, width = 74, height = 74 },
                textureRect = { x = 0, y = 228, width = 74, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_01.png",
                spriteColorRect = { x = 17, y = 23, width = 76, height = 72 },
                textureRect = { x = 74, y = 228, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_02.png",
                spriteColorRect = { x = 18, y = 23, width = 76, height = 72 },
                textureRect = { x = 150, y = 228, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_03.png",
                spriteColorRect = { x = 18, y = 23, width = 76, height = 72 },
                textureRect = { x = 0, y = 302, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_04.png",
                spriteColorRect = { x = 19, y = 21, width = 76, height = 74 },
                textureRect = { x = 76, y = 302, width = 76, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_05.png",
                spriteColorRect = { x = 19, y = 23, width = 76, height = 72 },
                textureRect = { x = 152, y = 302, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_06.png",
                spriteColorRect = { x = 19, y = 21, width = 76, height = 74 },
                textureRect = { x = 0, y = 376, width = 76, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_07.png",
                spriteColorRect = { x = 19, y = 23, width = 76, height = 72 },
                textureRect = { x = 152, y = 302, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_08.png",
                spriteColorRect = { x = 19, y = 21, width = 76, height = 74 },
                textureRect = { x = 76, y = 302, width = 76, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_09.png",
                spriteColorRect = { x = 18, y = 23, width = 76, height = 72 },
                textureRect = { x = 0, y = 302, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "02_10.png",
                spriteColorRect = { x = 18, y = 23, width = 76, height = 72 },
                textureRect = { x = 150, y = 228, width = 76, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_01.png",
                spriteColorRect = { x = 11, y = 23, width = 82, height = 72 },
                textureRect = { x = 76, y = 376, width = 82, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_02.png",
                spriteColorRect = { x = 12, y = 23, width = 82, height = 72 },
                textureRect = { x = 158, y = 376, width = 82, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_03.png",
                spriteColorRect = { x = 12, y = 23, width = 82, height = 72 },
                textureRect = { x = 0, y = 450, width = 82, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_04.png",
                spriteColorRect = { x = 11, y = 21, width = 84, height = 74 },
                textureRect = { x = 82, y = 450, width = 84, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_05.png",
                spriteColorRect = { x = 11, y = 23, width = 84, height = 72 },
                textureRect = { x = 166, y = 450, width = 84, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_06.png",
                spriteColorRect = { x = 11, y = 21, width = 84, height = 74 },
                textureRect = { x = 0, y = 524, width = 84, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_07.png",
                spriteColorRect = { x = 11, y = 23, width = 84, height = 72 },
                textureRect = { x = 84, y = 524, width = 84, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_08.png",
                spriteColorRect = { x = 11, y = 21, width = 84, height = 74 },
                textureRect = { x = 168, y = 524, width = 84, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_09.png",
                spriteColorRect = { x = 12, y = 23, width = 82, height = 72 },
                textureRect = { x = 0, y = 598, width = 82, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "03_10.png",
                spriteColorRect = { x = 12, y = 23, width = 82, height = 72 },
                textureRect = { x = 82, y = 598, width = 82, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_01.png",
                spriteColorRect = { x = 11, y = 15, width = 82, height = 80 },
                textureRect = { x = 164, y = 598, width = 82, height = 80 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_02.png",
                spriteColorRect = { x = 12, y = 17, width = 80, height = 78 },
                textureRect = { x = 0, y = 678, width = 80, height = 78 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_03.png",
                spriteColorRect = { x = 12, y = 17, width = 78, height = 78 },
                textureRect = { x = 80, y = 678, width = 78, height = 78 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_04.png",
                spriteColorRect = { x = 13, y = 19, width = 76, height = 76 },
                textureRect = { x = 158, y = 678, width = 76, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_05.png",
                spriteColorRect = { x = 17, y = 19, width = 72, height = 76 },
                textureRect = { x = 0, y = 756, width = 72, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_06.png",
                spriteColorRect = { x = 16, y = 21, width = 72, height = 74 },
                textureRect = { x = 72, y = 756, width = 72, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_07.png",
                spriteColorRect = { x = 14, y = 23, width = 74, height = 72 },
                textureRect = { x = 144, y = 756, width = 74, height = 72 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_08.png",
                spriteColorRect = { x = 16, y = 21, width = 72, height = 74 },
                textureRect = { x = 0, y = 832, width = 72, height = 74 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_09.png",
                spriteColorRect = { x = 13, y = 19, width = 76, height = 76 },
                textureRect = { x = 72, y = 832, width = 76, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_10.png",
                spriteColorRect = { x = 13, y = 17, width = 76, height = 78 },
                textureRect = { x = 148, y = 832, width = 76, height = 78 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_11.png",
                spriteColorRect = { x = 14, y = 19, width = 76, height = 76 },
                textureRect = { x = 0, y = 910, width = 76, height = 76 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
            {
                name = "04_12.png",
                spriteColorRect = { x = 12, y = 15, width = 80, height = 80 },
                textureRect = { x = 76, y = 910, width = 80, height = 80 },
                spriteSourceSize = { width = 120, height = 120 },
                spriteTrimmed = true,
                textureRotated = false
            },
        }
    }
    return sheet
end

