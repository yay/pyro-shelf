module(..., package.seeall)

require "screen"
require "sound"
require "bmf"
--require "inneractive"
--require "admob"

local CW = display.contentWidth
local CH = display.contentHeight
local SOX = display.screenOriginX
local SOY = display.screenOriginY
--local CSX = display.contentScaleX
--local CSY = display.contentScaleY

local CSX = screen.getCSX()
local CSY = CSX

-- localize these functions as they will be called a hundred of times inside a loop
local newScaledImage = screen.newScaledImage
local newString = bmf.newString
local font

local function createDoors()
    local lg = display.newGroup()

    local lDoor = newScaledImage("door_left")
    lDoor:setReferencePoint(display.CenterLeftReferencePoint)
    lDoor.x = SOX
    lDoor.y = CH / 2
    lDoor:scale(1, display.contentScaleY * screen.height / lDoor.contentHeight)

    local rDoor = newScaledImage("door_right")
    rDoor:setReferencePoint(display.CenterRightReferencePoint)
    rDoor.x = CW - SOX
    rDoor.y = CH / 2
    rDoor:scale(1, display.contentScaleY * screen.height / rDoor.contentHeight)

    local loading = newScaledImage("loading")
    loading:setReferencePoint(display.CenterReferencePoint)
    loading.x = CW / 2
    loading.y = CH / 2 - 100

    lg:insert(lDoor)
    lg:insert(rDoor)
    lg:insert(loading)

    return lg
end

local function createLevelButtons(worldIndex, doors, stars)
    local levelButtons = display.newGroup()

    local xBlocks = 5
    local yBlocks = 5

    local blockWidth  = display.contentWidth / xBlocks
    local blockHeight = display.contentHeight / yBlocks * 0.66

    local function onButtonTap(levelIndex)
--        inneractive.hideAd()
--        admob.hideAd()
        -- block further touches and taps
        levelButtons:insert(screen.newFullRect(true, true))
        sound.play("swish")
        -- slide buttons up
        transition.to(levelButtons, {time=333, delay=50, y=SOY*2-CH-200, onComplete=function()
            -- fade out stars
            transition.to(stars, {time=500, alpha=0, onComplete=function()
                -- fade in doors
                transition.to(doors, {time=500, alpha=1, onComplete=function()
                    director:changeScene( "level", worldIndex, levelIndex, true )
                end})
            end})
        end})
    end

    local function newButtonBlock(startIndex, blockIndex)
        local buttonBlock = display.newGroup()

        buttonBlock:insert(screen.newFullRect(false, true))

        local buttons = display.newGroup()

        local levels = settings.data.worlds[worldIndex].levels

        for i = 1, xBlocks do
            for j = 1, yBlocks do
                local levelIndex = (j - 1) * xBlocks + i + startIndex
--                local unlocked = levels[levelIndex] >= 0
                local unlocked = worldIndex <= 2 and levels[levelIndex] >= 0 or false
--                local unlocked = true -- TODO: DON'T FORGET TO REMOVE THIS!
                local completed = levels[levelIndex] > 0
                local imageSuffix
                if unlocked then
                    if completed then
                        imageSuffix = tostring(blockIndex)
                    else
                        imageSuffix = "_gray"
                    end
                else
                    imageSuffix = "_lock"
                end

                local levelButton = newScaledImage("level_button" .. imageSuffix)
                levelButton.x = blockWidth  * i - blockWidth  / 2
                levelButton.y = blockHeight * j - blockHeight / 2 + SOY
                buttons:insert(levelButton)

                if unlocked then
                    local levelText = newString(font, tostring(levelIndex))
                    levelText:setReferencePoint(display.CenterReferencePoint)
                    levelText:scale(CSX * 0.9, CSY * 0.9)
                    levelText.x = levelButton.x
                    levelText.y = levelButton.y
                    buttons:insert(levelText)
                end

                function levelButton:clean()
                    self:removeEventListener("tap", self)
                end

                function levelButton:tap()
                    onButtonTap(levelIndex)
                    return true
                end
                if unlocked then
                    levelButton:addEventListener("tap", levelButton)
                end
            end
        end

        buttons:setReferencePoint(display.CenterReferencePoint)
        buttons.y = CH / 2 - (screen.scaleFactor == 1 and 20 or 0)

        buttonBlock:insert(buttons)

        return buttonBlock
    end

    local slider = screen.newSlider()

    local slideCount = 4
    local dotSpacing = 64

    for i = 1, slideCount do
        local slide = newButtonBlock((i - 1) * 25, i)

        local slideButton = display.newGroup()
        slideButton.id = i
        local buttonOn = newScaledImage("dot_on")
        local buttonOff = newScaledImage("dot_off")
        buttonOff:scale(0.8, 0.8)
        slideButton:insert(buttonOn)
        slideButton:insert(buttonOff)
        slideButton:setReferencePoint(display.CenterReferencePoint)
        slideButton.x = (CW - (slideCount - 1) * dotSpacing) / 2 + (i - 1) * dotSpacing
        slideButton.y = CH / 2 + (screen.scaleFactor == 1 and 156 or 166) - SOY / 2

        slider:addSlide(slide, slideButton)
    end

    local backBtn = screen.newButton({
        default = "back",
        overScale = 1.2,
        onTap = function()
            sound.play("click")
            director:changeScene("worldmenu", worldIndex)
        end
    })
    backBtn:setReferencePoint(display.BottomLeftReferencePoint)
    backBtn.x = 10
    backBtn.y = CH - SOY - 10

    local worldText = screen.newScaledText("", 0, 0, settings.mainFont, 20)
    screen.updateText(worldText, "World #" .. tostring(worldIndex),
        CW - SOX - 10, CH - SOY - 34, display.BottomRightReferencePoint)

    local scoreText = screen.newScaledText("", 0, 0, settings.mainFont, 20)
    screen.updateText(scoreText, "Score: " .. tostring(settings.data.worlds[worldIndex].score),
        CW - SOX - 10, CH - SOY - 10, display.BottomRightReferencePoint)

    levelButtons:insert(slider)
    levelButtons:insert(backBtn)
    levelButtons:insert(worldText)
    levelButtons:insert(scoreText)

    return levelButtons
end

function new(worldIndex)
--    inneractive.hideAd()
--    inneractive.showAd(1, SOX, SOY)
--    admob.showAd(true)

    font = bmf.loadFont("LaBouf" .. screen.imageSuffix .. ".fnt")

    local menuGroup = display.newGroup()

    local stars = newScaledImage("main_bg")
    stars:setReferencePoint(display.TopCenterReferencePoint)
    stars.x = CW / 2
    stars.y = SOY

    local doors = createDoors()
    doors.alpha = 0

    menuGroup:insert(doors)
    menuGroup:insert(stars)
    menuGroup:insert(createLevelButtons(worldIndex, doors, stars))

    if worldIndex > 2 then
        local comingSoon = newScaledImage("coming_soon")
        comingSoon.x = CW / 2
        comingSoon.y = CH / 2
        menuGroup:insert(comingSoon)
    end

    return menuGroup
end

function clean()
    font:dispose()
end